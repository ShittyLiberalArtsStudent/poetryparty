FROM python:2.7.9
#-onbuild
WORKDIR /code
ENV PYTHONBUFFERED 1
RUN apt-get update -y
RUN apt-get -y --force-yes upgrade
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get install -y swig cmake
RUN apt-get install -y software-properties-common
RUN apt-add-repository ppa:python-pylinkgrammar/getsome
RUN apt-get install -y liblink-grammar4
RUN apt-get install -y liblink-grammar4-dev
RUN apt-get install -y python-enchant
# FIXME(ShittyLiberalArtsStudent) I don't *THINK* it's necessary to install system wordnet package
RUN apt-get install -y wordnet
RUN apt-get -y autoremove
RUN apt-get -y autoclean

# <SSH>
# you shouldn't run sshd in your containers. BUT GOD IS DEAD
RUN apt-get install -y ssh
RUN mkdir /var/run/sshd
RUN echo 'root:screencast' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
# </SSH>

# now try installing pylinkgrammar ahead of other things (fail fast)
RUN pip install pylinkgrammar
# must install yaml beforehand because old-ass version of NLTK depends on it
RUN pip install pyyaml
RUN pip install -U pip setuptools
# old-ass version of NLTK needs old version of setuptools
#   cf. https://github.com/nltk/nltk/issues/824
RUN pip install -U 'setuptools<10.0'
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD ./download_corpora.py /code/download_corpora.py
RUN python /code/download_corpora.py

RUN apt-get install -y nginx supervisor
RUN pip install uwsgi
# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf


# <LOCALE-BULLSHIT>
# fix "unsupported locale setting" error
# which was bubbling up from pylinkgrammar
# which tried to call: locale.setlocale(locale.LC_ALL,"en_US.UTF-8")
# which should work. so we have to make it work... thru some ugly fucking shit what the hell why
#   cf. http://askubuntu.com/questions/205378/unsupported-locale-setting-fault-by-command-not-found
RUN apt-get install locales -y

#ENV LANGUAGE en_US.UTF-8
#ENV LANG en_US.UTF-8
#ENV LC_ALL en_US.UTF-8
#ENV LC_CTYPE en_US.UTF-8
#ADD ./environment /etc/environment

RUN locale-gen en_US en_US.UTF-8
RUN dpkg-reconfigure locales

# holy fucking shit this part was hard to figure out. what the fuck.
# http://www.linuxquestions.org/questions/linux-general-1/locale-cannot-set-lc_all-to-default-locale-no-such-file-or-directory-218622/#post2915535
RUN localedef -v -c -i en_US -f UTF-8 en_US.UTF-8 || true
# verify -- print a euro symbol, then test python's ability to setlocale
RUN perl -Mcharnames=:full -CS -wle 'print "\N{EURO SIGN}"'
RUN python -c "import locale; locale.setlocale(locale.LC_ALL,'en_US.UTF-8')"

RUN cat /etc/default/locale
# </LOCALE-BULLSHIT>

# got error about missing "/usr/local/wordnet2.0/dict/lexnames" which is in this other dir.
ADD ./en/wordnet/wordnet2 /usr/local/wordnet2.0/

RUN rm -r /code
ADD . /code
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /code/nginx-app.conf /etc/nginx/sites-enabled/
#RUN ln -s /code/supervisor-app.conf /etc/supervisor/conf.d/

# run nginx as root because ughhhh permission
# (I did chmod and all that; the "css" directory remained forbidden forever)
RUN sed -i '/user www-data;/c\user root;' /etc/nginx/nginx.conf

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

ENV PYTHONPATH /code:/code/en
ENV DJANGO_SETTINGS_MODULE poetrynet.settings

RUN ls /code/supervisord.ini
RUN cat /code/supervisord.ini
CMD ["supervisord", "--nodaemon", "-c", "/code/supervisord.ini"]
