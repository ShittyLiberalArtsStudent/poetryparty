What is poetry.party? (2012)
=================================================================================
A multiplayer performance/game where you write poems together. It's wily, weird,
and very fun.


Don't look at the awful code (2015)
=================================================================================

This code is god-awful (it was my first big project), but the application is very fun.
It was originally written in 2011 for my undergrad thesis at New College of Florida.
The code is so bad because it was the first big coding project I did.

I don't plan on fixing this thing (it would need a rewrite, really) but it's functioning.
You can throw it in Docker containers with `docker-machine` and `docker-compose`.

It's ready to go, so run a few commands, then
write some weird poems in real time with your friends :D

How to run it (2015)
===================

Set up is easy using Docker containers.

Installation instructions for [Docker orchestration tools](http://blog.docker.com/2015/02/orchestrating-docker-with-machine-swarm-and-compose/):

* [Install Docker Machine](https://docs.docker.com/machine/)
* [Install Docker Compose](https://docs.docker.com/compose/)

(It was originally written totally coupled to DotCloud because they generously gave me free hosting,
as a student, for a year. I've just updated it so it's coupled to the Docker ecosystem now.)

Use `docker-machine` to make a host VM for your containers

    docker-machine create --driver boot2docker mydockerhost  # or whatever other driver you want.
    $( docker-machine env mydockerhost)

Build and run the containers:

    docker-compose build
    docker-compose up

That's it. `echo $DOCKER_HOST` will tell you the IP to connect to.
Instructions for playing are right in the application.
Connect in your browser, tell your friends to do the same, and play with words!


Thanks!
========

Thank
you

michael <sunbathersunbather .-~at~-. yahoo .-~dot~-. com>
