#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo "[~] postinstall DIR=$DIR"
app_name=poetrynet

setup_django() {
    python "$DIR/$app_name/manage.py" syncdb --noinput
    python "$DIR/$app_name/manage.py" collectstatic --noinput
    python "$DIR/$app_name/manage.py" check_permissions
    python "$DIR/init_db_make_admin.py"

}

setup_django

# Start the server!
exec gunicorn_django --bind 0.0.0.0:8000 --pythonpath="/code" --threads=4
