#!/usr/bin/env python

if __name__ == "__main__":
    import nltk
    nltk.download("wordnet")
    nltk.download("brown")
