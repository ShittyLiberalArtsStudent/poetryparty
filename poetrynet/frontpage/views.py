from poetrynet.project_utils import render_response
from poetrynet.poems.models import Poem


def home(request):
    """The index type page to greet a user. Hopefully one day it will display an activity stream
    or soemthing."""
    poems = Poem.objects.order_by('-created_date', '-pub_date', 'title')
    recent_poems = poems[:13]
    return render_response(request, 'frontpage/home.html', {"recent_poems": recent_poems})


def allpoems(request):
    poems = Poem.objects.order_by('-created_date', '-pub_date', 'title')
    return render_response(request, 'frontpage/allpoems.html', {"poems": poems})


def contact(request):
    return render_response(request, 'frontpage/contact.html')


def instructions(request):
    return render_response(request, 'frontpage/instructions.html')