import re

import en #nodebox linguistics
from pylinkgrammar import linkgrammar #grammar!
#from utils import lastWord
#from poetrynet.poems.corpus_utils import fetchCorpus
from poetrynet.poems.editor import EOS, rmEOS, stops2spaces, prettyJoin
from poetrynet.poems.english import bigram_ok
from poetrynet.poems.utils import Eme
from poetrynet.poems.aleatorics import RNG

from random import choice, shuffle

#####################
##HOUSEKEEPER$#######
#####################

class Feme(str):
    """A faux eme: used to turn bigram symbols into eme-like objects"""

    def set_text(self,text):
        self._text = text
    def get_text(self):
        return self._text
    text = property(get_text, set_text)
    
    def set_bm(self,bm):
        self._bm = bm
    def get_bm(self):
        return self._bm
    bm = property(get_bm, set_bm)

    def set_db(self,db):
        self._db = db
    def get_db(self):
        return self._db
    db = property(get_db, set_db)
    
    def __init__(self,data):
        self.bm = ""
        self.text = str(data)
        if data in EOS:
            self.bm = data
            self.text = data
        elif data == "C":
            self.bm = "C"
            self.text = "run"
        elif data[0] == "X":
            self.text = data.split("|")[1]
            self.bm = "X"
        self.db = str(data)

    def __str__(self):
        return str(self.text)
    def __unicode__(self):
        return unicode(self.text)


def lastWord(tokens):
    meep = []
    meep.extend(tokens)
    meep.reverse()
    for token in meep:
        if token:
            if token[-1].isalpha():
                return token

def lastBM(tokens):
    meep = []
    meep.extend(tokens)
    meep.reverse()
    for x in meep:
        x = Eme(x)
        if x.bm:
            return x
            

#####################
##INFLECTIONS STUFF##
#####################

#this is from en.verb.tenses(); TENSES_NO_S lacks the 3rd singular present so that we get "-s" less
TENSES_NO_S = set(['infinitive', 'present participle', 'past plural', '2nd singular present', '2nd singular past', 'past',
          'past participle', '1st singular present', '1st singular past', '3rd singular past', 'present plural'])
TENSES = set(['infinitive', 'present participle', 'past plural', '2nd singular present', '2nd singular past', 'past', '3rd singular present',
          'past participle', '1st singular present', '1st singular past', '3rd singular past', 'present plural'])

NEGATABLES = set(['shall', 'should', 'do', 'dare', 'be', 'have', 'can', 'could', 'need', 'may', 'ought', 'will', 'would', 'must', 'might'])

def conjugations(verb):
    """Given a verb, return the conjugations NodeBox Linguistics knows for it."""
    tail = ""
    if " " in verb:
        splut = verb.split()
        verb = splut[0]
        tail = " ".join(splut[1:]) #I don't know why I did this part of the code???
    def go(verb, negate=False):
        v = []
        xxx = TENSES_NO_S
        if RNG(50): xxx = TENSES
        for tense in xxx:
            try:
                conjugation = en.verb.conjugate(verb, tense, negate) + tail
                if conjugation:
                    v.append(conjugation)
            except KeyError: #<-- verb is undocumented
                pass
        return v
    v = go(verb)
    if en.verb.infinitive(verb) in NEGATABLES:
        v.extend( go(verb, True) )
    return v

def inflections(centreme):
    """Given a single content-word (WordNet word), return inflections & conjugations of it."""
    c = [centreme]
    chance = 100
    try:
        if en.is_adjective(centreme):
            chance = chance - 35
        if en.is_adverb(centreme):
            chance = chance - 35
    except:
        pass
    if RNG(chance): #if it's an adjective and/or an adverb, the chance of inflecting it decreases
        try:
            infinitive = en.verb.infinitive(centreme)
            if infinitive:
                if RNG(80):
                    c.extend( conjugations(infinitive) )
                else:
                    c.append( infinitive )
            else:
                if RNG(50): #50 percent chance of plurals
                    if centreme[-1]=='s':
                        singular = en.noun.singular(centreme)
                        pluralized = en.noun.plural(singular)
                    else:
                        pluralized = en.noun.plural(centreme)
                    c.append( pluralized )
        except:
            pass
        c = list( set(c) )
        shuffle(c)
    return c


#removed: finishSentence ...
        
def nextBMs(l, i=2):
    """From antemes/postemes, returns benchmarks (not in symbol form, but in actual eme form).
        If i=2 (default) it returns 2 of these (for the loop of bigram testing). But if i=1
        it returns just 1 (for initiating the bigram testing, using the last_benchmark)..."""
    gotten = []
    for x in l:
        #turn x into a feme if it has no attribute "bm"
        try: x.bm
        except AttributeError: x = Feme(x)
        if x.bm:
            if x.bm in EOS:
                gotten.append(x)
            elif x.bm[0]=='X':
                gotten.append(x)
            elif x.bm=='C':
                gotten.append(x)
            if len(gotten)==i:
                return gotten
    return gotten.extend(["C"])
            

def checkBigrams(feme, noncentreme):
    """Walk the anteme or posteme and remove bad eggs. This function knows what to do with a feme."""
    offset = 0
    q = 0

    try:
        while True:
            if feme:
                print "STARTED BIGRAM CHECKS -- FEME ZONE"
                a = feme
                b = None
                print "noncentreme", noncentreme
                nexties = nextBMs(noncentreme, 1)
                while not b:
                    try:
                        b = nexties.pop() #<the 1 is what makes the first one different 
                        q += 1
                        if q>10:
                            break
                    except:
                        break
                ok = bigram_ok(a, b)
                print " ", "Bigrams:", str(a), str(b), '- ok:', ok #debug print
                if not ok:
                    noncentreme.remove(b)
                else:
                    feme = None #trust me!!
            else:
                print "ONTO SECOND PHASE"
                
                q = 0
                a = None
                b = None
                while not a and not b:
                    nexties = nextBMs(noncentreme[offset:])
                    a = nexties[0]
                    b = nexties[1]
                    q += 1
                    if q>15:
                        break
                ok = bigram_ok(a, b)
                print " ", "Bigrams:", str(a), str(b), '- ok:', ok #debug print
                if not ok:
                    noncentreme.remove(b)
                else:
                    offset += 1
    except TypeError:
        return noncentreme
    return noncentreme

def cleanArticles(x, posteme):
    """Makes sure articles agree; also, replaces any "a" or "an" in posteme with "the" 
    Oh and X should be anteme + centreme."""
    
    VOWELS = 'aeiouAEIOU'
    
    y = [item.lower() for item in x]

    # XXX DEACTIVATED BASICALLY
    return x, posteme
    
    try:
        a = y.index('a')
        next_toke_index = a+1
        next_toke = x[next_toke_index]
        while next_toke[0].isspace():
            next_toke = x[next_toke_index]
            next_toke_index += 1
        if next_toke[0] in VOWELS:
            x[a] = 'an'
    except ValueError:
        try:
            an = y.index('an')
            next_toke_index = an+1
            next_toke = x[next_toke_index]
            while next_toke[0].isspace():
                next_toke = x[next_toke_index]
                next_toke_index += 1
            if next_toke[0] not in VOWELS:
                x[an] = 'a'
        except ValueError:
            pass
        
    posteme_lowered = [item.lower() for item in posteme]
    
    if posteme:
        art = None
        try:
            art = posteme_lowered.index('a')
        except ValueError:
            try:
                art = posteme_lowered.index('an')
            except ValueError:
                pass
        if art:
            posteme[art] = 'the'
    
    return x, posteme

def inflectAndEvaluate(last_benchmark, antecedent, anteme, centreme, posteme, capitalizings):
    """Given the raw text-to-be-morphed and also some INFLEXIBLE text preceding it, return all relevant inflections of the plastic text."""
    #initialize
    finality = 0 #<-- running percentage-chance that a period or newline will be inserted at the END OF THIS INPUT, automatically.
    last_word_is_centreme = False 
    before, after, safe_before, safe_after = "", "", "", ""
    seventy_percent_certainty = RNG(75)

    got = None

    #capitalize first letter, later on?
    if antecedent=="~":
        cap_start = True
        antecedent = " "
    else: cap_start = False
    
    #ANTEME
    if anteme:
        feme = Feme(last_benchmark)
        anteme = checkBigrams(feme, anteme)
        #real_anteme = prettyJoin(anteme, starter=antecedent)
        #flat_anteme = stops2spaces(real_anteme)
        foo = [antecedent]; foo.extend(anteme)
        before = prettyJoin(foo)
        before = stops2spaces(before)
        safe_before = rmEOS(before)
    else: anteme = []
    
    #POSTEME
    if posteme:
        feme = Feme(centreme)
        posteme = checkBigrams(feme, posteme)
        #real_posteme = prettyJoin(posteme, starter=centreme)
        #flat_posteme = stops2spaces(real_posteme)
        after = prettyJoin(posteme, starter=centreme)
        after = stops2spaces(after)
        safe_after = rmEOS(after)
    else: posteme = []
    
    #CREATE PARSER
    p = linkgrammar.Parser()
    j = 0 #overall parse-attempts ever
    
    #recurring piece of code
    def go(before, g, after):
        """
        Given an inflection of a centreme, try to parse the sentence; return that inflection if parseable.
        """
        variation = prettyJoin( [before, g, after] )
        linkages = p.parse_sent( variation )
        if linkages:
            print "LINKS!!!", variation
            return g
        else:
            print "NO LINKS:", variation
            return False #<-- returns False ONLY if none of them worked

    #store the variations on the centreme then do first attempt (no filler)
    centreme.inflections = inflections(centreme)
    for inf in centreme.inflections:
        got = go(before, inf, after)
        #j += 1
        if got:
            finality += 70
            break

    if not got:
        got = choice(centreme.inflections)
        print "NO RESULTS: TAKE RANDOM CHOICE FROM INFLECTIONS."
    #else:
        #print 'First go unsuccessful.'
        
        #attempt=1
        #the_tokens = []
        #the_tokens.extend(anteme)
        #the_tokens.append(centreme)
        #the_tokens.extend(posteme)
        #last_word = lastWord(the_tokens)
        
        #if last_word==centreme:
        #    last_word_is_centreme = True
            
        #while attempt<3 and j<4:
        #    print "Sentence filler attempt (this is #%s. )" % attempt
        #    
        #    dr = attempt >= 1
        #    phantom = finishSentence(last_word, corpNo=attempt, dire=dr)
        #    
        #    #copy inflections into a queue
        #    q = []
        #    q.extend(centreme.inflections)
        #    print "Q: ", q
        #
        #    try:
        #        while not got and j<4:
        #            inf = q.pop()
        #            print "Trying:" + inf
        #            if last_word_is_centreme:
        #                print "Last word is centreme ..."
        #                last_word = inf
        #                phantom = finishSentence(last_word, corpNo=attempt, dire=dr )
        #            #if attenuate_phantom:
        #            #    phantom = phantom.replace(last_word, '', 1)
        #            bef = before
        #            aft = "".join([after, phantom])
        #            got = go(bef, inf, aft)
        #            j += 1
        #    except IndexError:
        #        print "no more inf to try"
        #    
        #    if attempt>1:
        #        before = safe_before
        #        after = safe_after
        #    
        #    attempt += 1
        #    if got: break
            
    
    #PLACEHOLDER:
    #  do phrase_ok
    
    inflected_centreme = Feme(got)
    
    resulting_tokens = []
    resulting_tokens.extend(anteme)
    resulting_tokens.append(inflected_centreme)
    resulting_tokens, posteme = cleanArticles(resulting_tokens, posteme)
    resulting_tokens.extend(posteme)
    
    #print "JJJJJJ", j
    
    #do capitalizings
    for bah in capitalizings:
        try:
            resulting_tokens[bah] = resulting_tokens[bah].capitalize()
        except:
            print "Capitalization attempt failed."
    
    #finality
    if RNG(finality):
        final_bm = "%"
        resulting_tokens.append("\n")
    else:
        final_bm = lastBM(resulting_tokens)
        try: final_bm.bm
        except AttributeError: final_bm = Eme( str(final_bm) )
        final_bm = final_bm.bm #this property is the database-friendly version of the benchmark string
    
    results = prettyJoin( resulting_tokens, starter=antecedent, finalprint=True, flag=RNG(92) )
    
    print "Results: " + results
    
    if cap_start:
        if seventy_percent_certainty:
            rx = re.compile(r'[a-zA-Z]+')
            h = rx.search(results)
            results = results[h.start()].capitalize() + results[h.start()+1:]
    
    return results, final_bm