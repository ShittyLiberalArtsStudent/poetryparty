######################################
##ENGLISH PSEUDOLINGUSTICS CONSTANTS##
######################################

from poetrynet.poems.aleatorics import RNG

################################
## DETERMINERS (INCLUDES ARTICLES) [SEE http://en.wikipedia.org/wiki/Determiner_%28linguistics%29] 
################################
ALTERNATIVES = set(['another', 'other', 'different']) #removed: 'somebody else'
DEGREES = set(['many', 'much', 'few', 'little', 'several', 'most', 'very']) #removed: 'couple' / added: 'very'
DEMONSTRATIVES = set(['this', 'that', 'these', 'those', 'which'])
DISJUNCTIVES = set(['either', 'neither'])
DISTRIBUTIVES = set(['each', 'every'])
ELECTIVES = set(['any', 'either', 'whichever'])
ENOUGHS = set(['enough', 'sufficient', 'plenty']) #aka sufficiency determiners
EQUATIVES = set(['same'])
EVALUATIVES = set(['such', 'that', 'so'])
EXCLAMATIVES = set(['what', 'how']) #added: how
EXISTENTIALS = set(['some', 'any'])
NEGATIVES = set(['no', 'neither'])
PERSONALS = set(['we', 'you']) #<-- i.e. "we teachers, you guys" #IN OTHER WORDS: MUST HAPPEN BEFORE CENTREME TO BE RELEVANT ..
POSSESSIVES = set(['my', 'your', 'his', 'her', 'its', 'our', 'their', 'whose', 'thy']) #removed: 'own'
RELATIVES = set(['which', 'what', 'whichever', 'whatever']) #aka interrogatives
QUANTIFIERS = set(['all', 'few', 'many', 'several', 'some', 'every', 'each', 'any', 'no', 'little', 'only'])
UNIQUITIVES = set(['only', 'this', 'that', 'these', 'those']) #removed: the
UNIVERSALS = set(['all', 'both'])

#SO SPECIAL: ARTICLES
PRE_DET = set(['all','both','half','twice','thrice'])
PRE_ART = set(['such', 'what', 'quite',])
ARTICLES = set(['a', 'an', 'the', 'A', 'An'])
POST_ART = set(['rather'])
#COMBINATIONS OF THE ABOVE ...
DEFINITE_DET = set([]).union(DEMONSTRATIVES, DISTRIBUTIVES, EQUATIVES, EVALUATIVES, EXCLAMATIVES, PERSONALS, POSSESSIVES, UNIQUITIVES)
INDEFINITE_DET = set([]).union(ALTERNATIVES, DISJUNCTIVES, ELECTIVES, EXISTENTIALS, NEGATIVES, RELATIVES, UNIVERSALS)
Q_DET = set([]).union(DEGREES, QUANTIFIERS)
DET = set([]).union(DEFINITE_DET, INDEFINITE_DET, Q_DET, ENOUGHS)
#MORE SPECIALIZED/EXCEPTION-ORIENTED CATEGORIES
Q_DET_SAFE = Q_DET.difference( set(['no', 'some', 'any', 'all']) )
POST_PRE_DET = set([]).union(set(['different']), DEMONSTRATIVES, PERSONALS, POSSESSIVES.difference(['whose']))
POST_POSSESSIVES = set(['other', 'different'])
POST_EXCLAMADEMS = set(['many', 'few', 'different', 'little', 'very'])
PRE_SUCH = set(['another', 'many', 'no', 'how', 'that', 'only', 'few', 'other', 'several', 'all', 'another', 'what', 'which', 'whatever',
                'any', 'so'])
PRE_MUCH = set(['so', 'how', 'that', 'too', 'very', 'this'])
PRE_THAT = set(['some', 'several', 'any', 'only', 'that', 'those', 'neither', 'few', 'many']).union(EVALUATIVES, EXCLAMATIVES, PERSONALS)
DEM_MOD = DEMONSTRATIVES.difference(set(['that']))

NOT_EOS=set(['we', 'which', 'most', 'very', 'every', 'only', 'quite', 'so']).union(ARTICLES, ELECTIVES, NEGATIVES, POSSESSIVES,)

#A WHITELIST OF IDIOMATIC EXCEPTIONS
WHITELIST=set([ ('', ''), () ])

################################
##AND THE FUNCTIONS ...#########
################################


def bigram_ok(a, b):
    """A and B are proximal in a word sequence; return True if okay, False if not.
        ONLY FEED ME LOWER CASED THINGS."""
    
    a = str(a).lower()
    b = str(b).lower()
        
    a_det = a in DET
    b_det = b in DET
    b_art = b in ARTICLES
    if a in ARTICLES and b_art:
        return False
    elif a_det and b_det: #DET-DET
        b_qdetsafe = b in Q_DET_SAFE
        if a in ARTICLES: 
            if b_qdetsafe: #ARTICLE-Q_DET_SAFE
                return True
            elif b in POST_ART: # ARTICLE-POST_ART
                return True
            else: return False
        elif a in POSSESSIVES: 
            if b_qdetsafe: #POSSESSIVE-Q_DET_SAFE
                return True
            elif b in POST_POSSESSIVES: #POSSESSIVE-POST_POSSESSIVE
                return True
            else: return False
        elif a in PRE_ART and b_art: #PRE_ART-ARTICLE
            return True
        elif a in UNIVERSALS and b_art: #UNIVERSAL-ARTICLE
            return True
        elif a in PRE_DET and b in POST_PRE_DET: #PREDET-{POST_PRE_}DET
            return True
        elif b in POST_EXCLAMADEMS: #EXC.-POST_EXC., #DEM-POST_EXC
            if a in EXCLAMATIVES or a in DEMONSTRATIVES:
                return True
            else: return False
        elif b == 'such':
            if a in PRE_SUCH: return True
            else: return False
        elif b == 'that':
            if a in EXCLAMATIVES: return True
            if a in PRE_THAT:
                if RNG(50): #fifty-fifty on these ...
                    return True
                else:
                    return False
            else: return False
        elif b == 'much':
            if a in PRE_MUCH: return True
            else: return False
        else:
            return False
    elif a in PRE_DET and b in PRE_DET: #PRE_DET-PRE_DET
        return False
    elif a in PRE_ART and b in PRE_ART: #PRE_ART-PRE_ART
        return False
    else:
        return True
    
def phrase_ok(left_boundary, words, right_boundary="", prior_to_left=""):
    """ ONLY GIVE ME LOWER CASE.
        Left boundary is either a centreme (C or ADJ) or EOS marker (EOS), same w right boundary
            except some of the time you don't know what right_boundary is.
            words like "or" should probably be a boundary too ...
        Words are the words in between those events. """
    art_count = 0 #articles (only 1 per phrase)
    dem_count = 0 #dem_mod (demonstratives besides 'that' -- (only 1 per phrase)
    that_count = 0 # "that" (only 2 per phrase)
    pos_count = 0 #possessives (only 1 per phrase)
    if left_boundary=="J":
        #no determiners after an adjective
        new_words = []
        for word in words:
            if word not in DET and word not in ARTICLES:
                new_words.append(word)
            elif word == 'that' and RNG(50):
                new_words.append(word)
            else:
                if prior_to_left=="how" and word in ARTICLES:
                    new_words.append(word)
    else:
        new_words = []
        for word in words:
            if word in ARTICLES:
                art_count += 1
                if art_count<2:
                    new_words.append(word)
            elif word in DEM_MOD:
                dem_count += 1
                if dem_count<2:
                    new_words.append(word)
            elif word == 'that':
                that_count += 1
                if that_count<3:
                    new_words.append(word)
            elif word in POSSESSIVES:
                pos_count += 1
                if pos_count<2:
                    new_words.append(word)
            else:
                new_words.append(word)
    if right_boundary=="E":
        def EOS_ok(word):
            if word in NOT_EOS: return False
            else: return True
        satisfied = False
        i=1
        while not satisfied and len(words)>0:
            print words
            if EOS_ok(words[-1]):
                satisfied = True
            else:
                words.pop()
                i += 1
        new_words = words
    return new_words