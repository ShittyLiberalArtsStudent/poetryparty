from datetime import timedelta, datetime
import re
import random
import string
import traceback
from poetrynet.poems.models import Ping, Utterance

from retrofittedcounter import Counter
from poetrynet.poems.aleatorics import RNG, WRC
from editor import NEED_SPACE

##################################################################################
#GENERAL TOOLBOX##################################################################
######################################################%###########################
##################################################################################

def scale(x, mn, mx, a=0.0, b=9.0):
    """Scale a x in range [min, max] to occupy the same position in the range [a, b]. Note to self: give it floats."""
    top = (b-a) * (x-mn)
    bottom = mx - mn
    return float( ( top / bottom ) + a )

##WHITESPACE MANAGEMENT##

LINE_LENGTH_LIMIT = 60
TABSTOP = 4

def tokenize(text):
    """tokenize text into punctuation and letters and whitespace, preserving the whitespace.
    for reference on the regex used, see the stackoverflow thread that helped: http://bit.ly/p1rvqn
    Then I added the little |clause| for any punctuation. """
    p = re.compile(r"\d+|\*n|\*t|[%s]+|\b[-'A-z]+\b|[ ]+|\s+|[.,]+|\S+" % string.punctuation, re.I)
    slice_starts = [m.start() for m in p.finditer(text)] + [None]
    return [text[s:e] for s, e in zip(slice_starts, slice_starts[1:])]

def whitespace2html(text, tabstop=TABSTOP):
    """Change tabs to spaces, then spaces to nonbreaking spaces, then newline symbols to <br/>."""
    x = text.replace('\t', '&nbsp;'*tabstop).replace(' ', '&nbsp;').replace('\n', '<br/>')
    return x

def whitespace2plaintext(text, tabstop=TABSTOP):
    """Change tabs to spaces, then spaces to nonbreaking spaces, then newline symbols to <br/>."""
    return text.replace('&nbsp;'*tabstop, ' '*tabstop).replace('&nbsp;', ' ').replace('<br/>', '\n')

def quickEscape(text, tabstop=TABSTOP):
    """Changes sequences of n spaces to the plaintext tab symbol, \t
    Also changes *t to \t. Also changes *n, *br to \n"""
    return text.replace(' '*tabstop, '\t').replace('*t', '\t').replace('*n', '\n').replace('*br', '\n')

def normalizeWhitespace(string, preceding_character=""):
    """This helps our results be prettier by doing things like inserting spaces after commas if there isn't already one, etc."""
    #print 'The text given to normalizeWhitespace is ...', string*10
    #initialize
    i = 0
    results = []
    last_token = ""
    tokens = tokenize(string)
    length = len(tokens)
    #do the non-beginning
    while i<length:
        if last_token:
            if not last_token.isspace():
                if tokens[i][0].isalnum():
                    results.append(' ')
            elif last_token[-1:] in NEED_SPACE:
                if not tokens[i][0].isspace() and tokens[i][0] not in NEED_SPACE:
                    results.append(' ')
            elif last_token[-1:]=="'" or last_token[-1:]=='"':
                if RNG( 25 ):
                    results.append(' ')
        results.append( tokens[i] )
        last_token = tokens[i]
        i += 1
    #handle beginning
    if preceding_character:
        if not preceding_character.isspace():
            if tokens[0][0].isalnum():
                spacer = WRC([' ', '\t', '\n', '\n\n'])
                results.reverse()
                results.append(spacer)
                results.reverse()
    return "".join(results)

##ETC##

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]
        
def calculateMaxUtterances(usercount):
    """This determines what the appropriate max_utterances is based on dividing the usercount in half and rounding up,
    but also has a minimum value (max_utterances is minimum 2 until at least 3 users are involved. No MAX as of yet."""
    #if usercount>2:
    #    from math import ceil
    #    result = ceil(usercount/3.)
    #else:
    #    result = 2
    return 2

##################################################################################
#CLIENT-RELATED UTILS#############################################################
##################################################################################
##################################################################################

#def permission(user):
    #check later: can GUARDIAN take care of checking whether a user is invited
    #placeholder: 1) check if customized (if not, there is no max-user count)
    #            2) if invite-only, check if user is "on the guest list"
    #            3) if not invite-only, check current user count against max
    #                return FALSE, why            if max ever or max current has been exceeded.
    # 4) or, no excess of users, proceed w/o a hitch, returning TRUE.
#    return True

def initialPing(request, poem):
    """A ping sets/creates record relating request.user to this poem[stats] object, updating the time.
    Code just like this is found in the POLL view, take this INITIAL ping to be a (nearly-exact) model/duplicate. I know, it's not proper, whatever ..."""
    ping, created = Ping.objects.get_or_create(user=request.user, poemstats=poem.stats) #put this user on the active users list
    ping.date = datetime.now()
    ping.save()
    
def flooding(user):
    """First, adds one to the count of "recent" user submissions ...
    then checks if the count ATM is too high, and returns True if so."""
    longago = datetime.now()-timedelta(seconds=2)    
    #try:
    if True:
        ut = Utterance.objects.filter(user=user, date__gt=longago)
        if ut.count():
            return ut.count()
        else:
            return False
    #except:
    #    return False
#    try:
#        ut = Utterance.objects.filter(user=user, date__lt=thirty_seconds_ago)
#        return True
#    except:
#        return False
    
##################################################################################
#SPECIFICALLY RELATED TO ENSCRIBING UTTERANCES, NLTK STUFF, ETC.##################
##################################################################################
##################################################################################

import fuzzy
import Levenshtein
from nltk.corpus import wordnet as wn

#Parameters
X_EME_LENGTH = 8 # <-- length to pad/trim all antemes and postemes to.

#Vagueness demotion scores; first degree is THE WORST
FIRST_DEGREE_VAGUES = set(['abstract_entity', 'abstraction', 'act', 'action', 'entity', 'physical_entity', 'living_thing', 
                       'object','causal_agent', 'matter', 'purine', 'chemical_element', 'letter_of_the_alphabet', ])
SECOND_DEGREE_VAGUES = set(['solid', 'thing' 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 's', 't', 'v', 'w',])
THIRD_DEGREE_VAGUES = set(['whole', 'vertebrate', 'organism', 'person','artifact', 'roman_alphabet', 'latin_alphabet'])
#and so on ...
FOURTH_DEGREE_VAGUES = set(['animal','food','instrumentality','chordate'])
FIFTH_DEGREE_VAGUES = set(['go', 'move', 'element', 'locomote'])
SIXTH_DEGREE_VAGUES = set(['make', 'travel', 'have', ])

#Problem lemmas to remove completely:
PROBLEM_LEMMAS = set(['1', 'i', 'blood_type', 'blood_group', 'type_o', 'group_o', 'LOX', 'lox', 'liquid_oxygen', 'oregon', 'nobelium', 'noh', 'doctor_of_osteopathy', 'doh', 'ut', 'more', 'thomas_more', 'sir_thomas_more', 'political_campaign', 'charm_campaign', 'gay_lib', 'fund-raising_campaign', "women's_lib", 'youth_crusade', 'feminist_movement', 'lost_cause', 'electioneering', 'ad_campaign', 'reform', 'advertising_campaign', 'gay_liberation_movement', 'campaigning', 'fund-raising_drive', 'feminism', 'fund-raising_effort', 'youth_movement',  "women's_liberation_movement"])

#This magical contractions table is based on this webpage: http://bit.ly/q5JWzU
CONTRACTIONS = set(["aren't", "can't", "couldn't", "didn't", "doesn't", "don't", "hadn't", "hasn't", "haven't", "he'd", "he'll", "he's", "I'd", "I'll", "I'm", "I've", "isn't", "let's", "mightn't", "mustn't", "shan't", "she'd", "she'll", "she's", "shouldn't", "that's", "there's", "they'd", "they'll", "they're", "they've", "we'd", "we're", "we've", "weren't", "what'll", "what're", "what's", "what've", "where's", "who'd", "who'll", "who're", "who's", "who've", "won't", "wouldn't", "you'd", "you'll", "you're", "you've", "cannot", "gimme", "gonna", "gotta", "lemme", "ain't", 'none'])
DECONTRACTIONS = set(["are not", "can not", "could not", "did not", "does not", "do not", "had not", "has not", "have not", ("he had", "he would"), ("he will", "he shall"), ("he is", "he has"), ("I had", "I would"), ("I will", "I shall"), "I'm", "I have", "is not", "let us", "might not", "must not", "shall not", ("she had", "she would"), ("she will", "she shall"), ("she is", "she has"), "should not", ("that is", "that has"), ("there is", "there has"), ("they had", "they would"), ("they will", "they shall"), "they are", "they have", ("we had", "we would"), "we are", "we have", "were not", ("what will", "what shall"), "what are", ("what is", "what has"), "what have", ("where is", "where has"), ("who had", "who would"), ("who will", "who shall"), "who are", ("who is", "who has"), "who have", "will not", "would not", ("you had", "you would"), ("you will", "you shall"), "you are", "you've", "can not", "give me", "going to", "got to", "let me", "is not", 'not one'])
CONTRACTKILLER = dict(zip(CONTRACTIONS, DECONTRACTIONS))
#These sets are all stopwords in some sense but these lists overlap with the stopword lists too, they're for different operations ...
#DETERMINERS
ARTICLES = set(['a', 'an', 'the', 'some'])
#This stopwords list is based on the WordNet stopwords list (on Nov. 8th, 2011) plus my own research.
STOPWORDS = set(['who', 'in', 'whoever', 'via', 'through', 'yourselves', 'ah', 'o', 'thyself', 'its', 'whose', 'herself', 'should', 'to',
                 'whatsoever', 'ours', 'versus', 'them', 'his',  'thee', 'ye', 'self', 'whichever', 'they',
                 'despite', 'during', 'him', 'nor', 'hisself', 'this', 'she', 'where', 'whomever', 'idem', 'because', 'whosoever', 'whomso', 'notwithstanding',
                 'our', 'ourselves', 'what', 'for', 'since', 'while', 'per', 'neither', 'we', 'who', 'so', 'whomsoever', 'the',
                 'were', 'whichsoever', 'although', 'hers', 'thou', 'of', 'against', 'ourself', 
                 'thine', 'onto', 'or', 'among', 'into', 'whatever', 'yourself', 'everyone', 'everything',
                 'amidst', 'your', 'everybody', 'from', 'her', 'their', 'anyone', 'no',
                 'whom', 'themselves', 'circa', 'was', 'until', 'himself', 'that', 'tother', 
                 'somewhat', 'with', 'than', 'those', 'he', 'me', 'myself', 'these', 'us', 'will',
                 'wherewith', 'theirs', 'toward', 'my', 'and', 'it', 'an', 'as', 'itself', 'whatall',
                 'at', 'in',  'if', 'whereas', 'when', 'beside', 'how', 'which', 'near', 'you', 'many',
                 'towards', 'unless', 'oneself', 'I', 'after', 'upon', 'amongst', 'a', 'i', 'amid', 'anybody', 'yours', 'without', 'whether'])
#the following lists are things sorted out from the master STOPWORDS list ... all of the things below HAVE a translatable meaning, but also sometimes need to be treated as stopwords.
COULDA_WOULDA_SHOULDA = set(['would', 'should', 'shall', 'could'])
BE_VARIANTS = set(['am', 'be', 'being', 'is', 'are', 'were', 'was', 'been',])
DO_VARIANTS = set(['do', 'does', 'did', 'doing', 'done'])
HAVE_VARIANTS = set(['has', 'have', 'having', 'had'])
DESPITE_VARIANTS = set(['however', 'nevertheless', 'withal', 'still', 'yet', 'all the same', 'nonetheless', 'notwithstanding'])
POSITIONALS = set(['above', 'between', 'underneath', 'beneath', 'behind', 'opposite', 'out', 'beyond', 'off', 'yon', 'yonder', 'along', 'on', 'by', 'about',
                   'aboard', 'alongside', 'here', 'there', 'under', 'around', 'outside', 'down', 'up', 'across', 'inside', 'below', 'astride',])
NEGATIONALS = set(['opposite',  'not', 'excepting', 'excluding', 'barring', 'anti', 'except', 'no', ])
POSSIBLY_STOPWORDS = set(['is', 'are', 'only', 'then', 'am', 'had', 'once', 'nothing', 'naught', 'aught', 'each', 'both', 'over', 'being', 'all', 'just',
                          'very', 'like', 'besides', 'why', 'most', 'many', 'much', 'now', 'either', 'likewise',  'like', 'own', 'very', 'will', 'sundry', 
                          'though', 'some', 'past',  'while', 'within', 'suchlike', 'round', 'twain', 'plus', 'minus', 'another', 'anything', 'too',
                          'unlike', 'somebody', 'someone', 'nobody', 'more',  'but', 'mine', 'throughout', 'save', 'before', 'other', 'otherwise', 'something', 
                          'such', 'either','any', 'again', 'same', 'besides', 'must', 'different', 'few', 'enough', 'several', 'sufficient',
                          'quite', 'every', 'plenty', 'little', 'thy', 'might', "may", "will", "can", "opposite", 'excepting', 'including',
                          'excluding', 'barring', 'considering', 'pending', 'regarding', 'concerning', 'following', 'bar',
                          ]).union(COULDA_WOULDA_SHOULDA, BE_VARIANTS, DO_VARIANTS, HAVE_VARIANTS, POSITIONALS, DESPITE_VARIANTS)
# these words can be similar to 'despite' and should tend to be treated as stopwords. but again, if they are the only content word, they should be treated as a content word.
# there are some positionals left in the STOPWORDS list because they're so frequently used. these are more variable.

STOPWORDS_HOLDOVER = set([]).union(STOPWORDS, POSSIBLY_STOPWORDS, NEGATIONALS)

#Kinds of Emes. Anything above 0 is treated as a Nonword -- as punctuation (i.e. discrete symbols without translatable meaning).
PUNC_X = {
             r'x': -0.1, #Custom null-character ... absence should be meaningful!
             #r' ': 0, #single space
             r'    *t*ht*': 1, #space ... tab symbol (\t or *t or *ht) or tab amount of spaces (4)
             r'*n': 2, #newline symbol (\n or *n or *br or *r)
             r"(){}[]<>": 3, #containers + '
             r"""/\|-@#$%"'^*`~_""": 4, #misc + "
             r'&=@+123456789': 5, #ampersand and other conjunction-like marks -- but also {{Contractions}}, {{Stopwords}}
             r',': 6, #commas -- but also {{Proper Nouns}}, {{Alphanumeric Sequences}}
             r';': 7,
             r':': 8,
             r'...!?!!!???': 9,
         }

PUNC_TRANS = {9.0: ['.', '...', '!', '?', '!?', '!!!', '???'],
                        8.0: [':', ';', '.',],
                        7.0: [';', '.', ','],
                        6.0: [';', ','],
                        5.0: [',', '    '],
                        4.0: [',', ' ', '    '],
                        3.0: [' '],
                        2.0: [' '],
                        1.0: [' '],
                        0.0: ['']
                        }

Y = (
         (0, 'Only occurrence'),
         (1, 'General coincidence'),
         (2, 'Proximal coincidence'),
         (3, 'Positional coincidence'),
         (4, 'Double positional coincidence (probably wont get used)'),
         )
KINDS = (
         (0, 'Content word or *w'),
         (1, 'Contraction'),
         (2, 'Stopword'),
         (3, 'Proper Noun'),
         (4, 'POSSIBLY stopword.'),
         (5, 'Punctuation'),
         )

def intersection(first, *others):
    """Custom intersection function"""
    return set(first).intersection(*others)

def combo_ratio(string1, string2):
    """ A marriage of two similarity metrics; weighs more heavily on Levenshtein.
        I tried several other ways of combining these two, and they all had pretty similar results
        when testing on THOUSANDS of possibly-similar words.
        (as long as they still weighed Levenshtein more, which seems best ...) """
    #j = Levenshtein.jaro(string1, string2)
    #l = Levenshtein.ratio(string1, string2)
    #return ((l*2.0)+j)/2     
    #changing this to optimize some speed whatever
    return Levenshtein.ratio(str(string1), str(string2))

def spellcheck(word):
    """This isn't for VALIDATING words, but rather, for generating suggestions for the user based on a problematic one."""
    import enchant
    en_US = enchant.Dict("en_US")
    en_GB = enchant.Dict("en_GB")
    possibilities = set(en_US.suggest(word)+en_GB.suggest(word))
    results = ''
    for possibility in possibilities:
        if wn.synsets(possibility):
            results = results + possibility + ', '
    return results[:-2]

def pronounce(word=''):
    """Returns 3 phonetic versions of the word:
        nysiis (the most verbose),
        double metaphone (represents in just 4 characters, currently the most efficient for databases and stuff it seems like)
        soundex (the original, pretty fuzzy and odd)"""
    try:
        d =  fuzzy.DMetaphone()
        x = fuzzy.Soundex(8)
        one, two, three = fuzzy.nysiis(word), d(word), x(word)
        two = two[0] #drop the second item for now, i don't know what it is.
        #            #it's usually None, sometimes it's a different but similar result
        #also, weirdly enough, fuzzy capitalizes our string/word IN PLACE somehow,
        #so we have to reverse that ...
        word = word.lower()
        return one, two, three
    except:
        return False

def classify_punctuation(string):
    """Pass it a punctuation token. First, see if it has a verbatim score for it.
    Otherwise, it averages (and rounds) the PUNC_X values for all of the single characters.
    In both cases it returns some form of the original string, for the sake of building a pool of
    possible resulting punctuation-marks. In the first case the pool would just be verbatim symbol.
    In the second case it is a list(set()) of all marks used and the entire token too.
    * I thought about trying to detect poorly formed metacharacters, but fuck it, they should just
    be NOT understood, and treated as non-meta-character punctuation marks."""
    for key, value in PUNC_X.iteritems():
        if string in key:
            return value, set(string)
    pool = [string]
    sum = 0
    count = 0
    for character in string:
        count+=1
        pool.append(character)
        for key, value in PUNC_X.iteritems():
            if character in key:
                sum+=value
    return float(sum/count), pool

class Eme(str):
    """My own special Word/Nonword object."""
    
    def set_valid(self,valid):
        self._valid = valid
    def get_valid(self):
        return self._valid
    valid = property(get_valid, set_valid)
    
    def set_user(self,user):
        self._user = user
    def get_user(self):
        return self._user
    user = property(get_user, set_user)

    def set_message(self,message):
        self._message = message
    def get_message(self):
        return self._message
    message = property(get_message, set_message)

    def set_text(self,text):
        self._text = text
    def get_text(self):
        return self._text
    text = property(get_text, set_text)
    
    def set_synsets(self,synsets):
        self._synsets = synsets
    def get_synsets(self):
        return self._synsets
    synsets = property(get_synsets, set_synsets)

    def set_inflections(self,inflections):
        self._inflections = inflections
    def get_inflections(self):
        return self._inflections
    inflections = property(get_inflections, set_inflections)

    def set_capitalizing(self,capitalizing):
        self._capitalizing = capitalizing
    def get_capitalizing(self):
        return self._capitalizing
    capitalizing = property(get_capitalizing, set_capitalizing)

    def set_punc_value(self,punc_value):
        self._punc_value = punc_value
    def get_punc_value(self):
        return self._punc_value
    punc_value = property(get_punc_value, set_punc_value)

    def set_punc_pool(self,punc_pool):
        self._punc_pool = punc_pool
    def get_punc_pool(self):
        return self._punc_pool
    punc_pool = property(get_punc_pool, set_punc_pool)

    def set_bm(self,bm):
        self._bm = bm
    def get_bm(self):
        return self._bm
    bm = property(get_bm, set_bm)

    def set_kind(self,kind):
        self._kind = kind
    def get_kind(self):
        return self._kind
    kind = property(get_kind, set_kind)
    
    def __isCapitalizing(self):
        #presently the only way to cast capitalization is Using Title Caps (i.e. CAPital won't do anything.)
        if not self.text.islower():
            self.capitalizing = True
    
    def __isPunctuation(self, data):
        if not data.isalnum():
            data = quickEscape(data)
            self.text = data
            self.kind = KINDS[5]
            self.punc_value, self.punc_pool = classify_punctuation(data)
            if '.' in self.text:
                self.bm = '.'
            elif ';' in self.text:
                self.bm = ';'
            elif '!' in self.text:
                self.bm = '!'
            elif '?' in self.text:
                self.bm = "?"
            elif '%' in self.text:
                self.bm = '%'
            else:
                self.bm = None
            return True
        elif data.isdigit():
            #if it's all digits, that's okay. treat it as a stopword tho.
            self.text = data
            self.kind = KINDS[2]
            return True
        else:
            return False 

    def __isStopword(self,data):
        if data.lower() in STOPWORDS_HOLDOVER:
            self.text = data
            self.kind = KINDS[2]
            if data.lower() in POSSIBLY_STOPWORDS:
                self.kind = KINDS[4]
            self.__isCapitalizing()
            return True
        else:
            return False

    def __isContraction(self,data):
        if data.lower() in CONTRACTIONS:
            self.text = data
            self.kind = KINDS[1]
            self.__isCapitalizing()
            #here could go a setting of the DECONTRACTED word or something, but for now, lets just imagine we'll later create a method that unpacks the word according to the context it is called in ...
            #BTW mostly these decontract TO stopwords, which is v interesting. that will be accounted for ...
            return True
        else:
            return False

    def __isEme(self,data):
        #isContentWord?
        if self.__isStopword(data):
            self.bm = "X|" + data
            return True
        elif self.__isContraction(data):
            self.bm = "X|" + data
            return True
        else:
            synsets = wn.synsets(data)
            if synsets:
                self.text = data
                self.kind = KINDS[0]
                self.__isCapitalizing()
                self.bm = "C"
                return True
            else:
                #if self.__isProperNounEtc(data):
                #    return True
                if self.__isPunctuation(data):
                    return True
                else:
                    self.text = data
                    return False
    
    def __init__(self,data):
        self.inflections = None
        self.bm = None
        self.capitalizing = False
        self.punc_pool = []
        self.valid = self.__isEme(data)
        if not self.valid:
            suggestions = spellcheck(data)
            if suggestions:
                self.message = ( 'Spellcheck results for "%s": %s' % ( data,  suggestions) )
            else:
                self.message = ('Could not decipher "%s". '%data)

    def __str__(self):
        return str(self.text)
    def __unicode__(self):
        return unicode(self.text)
    

class UtteranceCucumber:
    """My own special Utterance object that will get pickled etc."""

    def set_valid(self,valid):
        self._valid = valid
    def get_valid(self):
        return self._valid
    valid = property(get_valid, set_valid)

    def set_user(self,user):
        self._user = user
    def get_user(self):
        return self._user
    user = property(get_user, set_user)

    def set_message(self,message):
        self._message = message
    def get_message(self):
        return self._message
    message = property(get_message, set_message)

    def set_remainder(self,remainder):
        self._remainder = remainder
    def get_remainder(self):
        return self._remainder
    remainder = property(get_remainder, set_remainder)

    def set_text(self,text):
        self._text = text
    def get_text(self):
        return self._text
    text = property(get_text, set_text)

    def set_anteme(self,anteme):
        self._anteme = anteme
    def get_anteme(self):
        return self._anteme
    anteme = property(get_anteme, set_anteme)

    def set_centreme(self,centreme):
        self._centreme = centreme
    def get_centreme(self):
        return self._centreme
    centreme = property(get_centreme, set_centreme)

    def set_posteme(self,posteme):
        self._posteme = posteme
    def get_posteme(self):
        return self._posteme
    posteme = property(get_posteme, set_posteme)

    def set_capitalizing(self,capitalizing):
        self._capitalizing = capitalizing
    def get_capitalizing(self):
        return self._capitalizing
    capitalizing = property(get_capitalizing, set_capitalizing)

    def set_punc_pool(self,punc_pool):
        self._punc_pool = punc_pool
    def get_punc_pool(self):
        return self._punc_pool
    punc_pool = property(get_punc_pool, set_punc_pool)
        
    def __init__(self,data, user_id):
        
        self.text = data
        self.user = user_id
        self.anteme = []
        self.centreme = None
        self.posteme = []
        self.capitalizing = []
        self.punc_pool = []
        self.remainder = '' #remainder to be returned to user's talkbox: by default, none.
        self.message = None
        self.valid = True
        ps_bool = False
        ps_index = 0
        i = 0
        if self.text and len(self.text)>=1:
            for token in tokenize(data):
                eme = Eme(token)
                eme.user = self.user
                 #if any valid eme is capitalized, take note ...
                if eme.capitalizing:
                    self.capitalizing.append(i)
                if eme.valid: #no complaints from the eme
                    if eme.kind[0]==0: #if it's a content-word (centreme)...
                        if not self.centreme: #stage==1 would mean this is the first content-word found...
                            self.centreme=eme #so it is the centreme of this utterance.
                            #stage=2 #then, we move to stage 2.
                        else: #centreme has already been assigned but this eme is also a content-word -- stop right there, no more emes!
                            self.remainder = data[data.find(str(eme)):] #remainder to be returned to user's talkbox
                            self.text = data[:data.find(str(eme))] #thus the TEXT of this utterance is the other half of this data
                    else:
                        #antreme/posteme dichotomy: core stuff
                        if not self.centreme:
                            self.anteme.append(eme)
                        else:
                            self.posteme.append(eme)
                        #extra things for punctuation, and for possibly-stopwords
                        if eme.kind[0]==5: #is punctuation
                            self.punc_pool.extend(eme.punc_pool)
                        elif not ps_bool and eme.kind[0]==4:
                            ps_bool = True
                            ps_index = i
                else: #if any emes aren't valid, the whole thing is invalid.
                    self.valid = False
                    self.message = eme.message
                    self.remainder = data #put the same stuff back into the talkbox
                i += 1
                if self.remainder:
                    break #if remainder has been assigned, stop moving forward in processing this utterance.
            #for loop has finished. move on
            if not self.centreme: #has not yet been declared invalid, so we must evaluate.
                if not ps_bool: #usually, not ps_index.
                    self.valid=False #there must be one content-word per input
                    if not self.message: #if self.message has already been set (i.e. to the message on an invalid eme), don't overwrite it
                        self.message=('Please add on a word or two and try again (need more content-words).')
                        self.remainder = self.text
                else:
                    #okay so there's no centreme, but ps_index has been set to the index where a possibly-stopword-possibly-centreme is.
                    #also, because there was no clear centreme, everything got put into anteme. so we have to do some shufflin.
                    swap = self.anteme
                    if len(swap)>1:
                        self.anteme = swap[:ps_index]
                        self.centreme = swap[ps_index]
                        self.posteme = swap[ps_index+1:] #i think this was slightly broken until I just added that +1 thing.
                    else:
                        #in this case there is only 1 eme at all and that's the possibly-stopword / now CENTREME.
                        self.anteme = []
                        self.centreme = swap.pop()
                        self.posteme = []
            else: #there IS a centreme; we have to do a little final round-up thing here
                try:
                    if len(self.text)==0 or not self.text:
                        a = "".join(self.anteme)
                        b = self.centreme
                        c = "".join(self.posteme)
                        self.text = a + b + c
                except:
                    pass
        else:
            self.valid=False
            self.message="Please try entering your input again. If this message persists for some reason, please modify your input slightly and try again."
            self.remainder = " " + self.text + " "
                

    def __str__(self):
        return str(self.text)
    def __unicode__(self):
        return unicode(self.text)



def fetch_names(synsets=[]):
    """Return all lemmas (in .name form) contained in a list of synsets"""
    lemmas = []
    for synset in synsets:
        lemmas += [x.name for x in synset.lemmas]
    return lemmas

def ripple(synsets=[]):
    """This does the actually breeding of the word ... including the specials area. It takes synsets and does them things [that colony/firstwave used to do, but this exists to reduce redundancy in the code. Returns result of get_names(alltheshit)."""
    original_synsets = synsets
    pile = synsets

    #I was getting some RIDICULOUS errors until I separated the for loops, separated the SYNSETS variables, etc ...
    #LATER ON, WE WILL STILL HAVE TO REFINE THIS RIPPLE FUNCTION, WHICH IS REALLY THE HEART OF THE EXPLOSION ALGORITHM!!!
    #HOW THIS WORKS, AND ALSO HOW MANY RIPPLE(RIPPLE(RIPPLE 's we do, REALLY DEFINES THE ALGORITHM.....
    for each_synset in original_synsets:
        pile = list(set(pile))
        pile += each_synset.hypernyms()
    for each_synset in original_synsets:
        pile = list(set(pile))
        pile += each_synset.hyponyms()
    for each_synset in original_synsets:
        pile = list(set(pile))
        pile += each_synset.member_holonyms()
    #for each_synset in original_synsets:
    #    pile = list(set(pile))
    #    pile += each_synset.root_hypernyms()
        
    #results = fetch_names(list(set(pile)))
    results = list(set(pile))

    #extra_lemmas = []
    #extra_lemmas_names = []
    #for each_synset in original_synsets:
    #    for each_lemma in each_synset.lemmas:
    #        extra_lemmas += each_lemma.antonyms() + each_lemma.derivationally_related_forms() + each_lemma.pertainyms()
    #        extra_lemmas_names += [lemma.name for lemma in extra_lemmas]
    #results += extra_lemmas_names

    results = list(set(results))
    return results

def explode(synset, specials=True):
    """This is effectively firstwave^2. Where we really blow up the colony started by some single word. Returns huuuuge list of names. """
    first_gen = ripple(synset)
    if RNG(50):
        second_gen = []
        for each_synset in first_gen:
            second_gen += ripple(wn.synsets(each_synset.name))
        everything = first_gen + second_gen
    else:
        everything = first_gen
    return everything

def commix(centremes, current_code=5):
    """No real docstring yet, but here are some notes.
        * _MUST_ return results as a weird form of dictionary; if dictionary were like the following,
        results = {'0': [item, item, item], '1': [item, item, item]} etc -- i.e. {'N: RANK': [items, with, this, rank]}
         then you do list_of_results = results.items(); list_of_results.sort() and return THAT.
    """
    print "Centremes fed into commix -- %s" % str(centremes)
    print "commix()", 0, locals()
    results = []
    
    def convoluteForSpecificity(centremes, returnScores=False):
        """This is the abstraction from convoluteForHighestSpecificity & convoluteForArbitrarySpecificity."""
        synsets = []
        lemmas = []
        lowest_seen = 10.0
        highest_seen = 1.0
        for centreme in centremes:
            synsets.extend( explode( wn.synsets(centreme) ) )
        for synset in synsets:
            rating = (synset.max_depth()+synset.min_depth())/2
            if returnScores:
                if rating<lowest_seen:
                    lowest_seen = rating
                elif rating>highest_seen:
                    highest_seen = rating
            lemmas.extend( [(rating, x.name.lower()) for x in synset.lemmas] )
            #not being used right now but could be useful -- x.count()
        print "commix()", 1, locals()
        counta = Counter(lemmas)
        if not returnScores:
            return counta
        else:
            return counta, lowest_seen, highest_seen

    def convoluteForHighestSpecificity(centremes):
        print "convoluteForHighestSpecificity ..." #DEBUG PRINT

        counta = convoluteForSpecificity(centremes)
        results = {}
        for item in counta.items():
            (rating, name), count = item
            rank = count*rating + (count-1)*2
            try:
                results[rank].append(name)
            except KeyError:
                results[rank] = [name]
        list_of_results = results.items()
        list_of_results.sort()
        print "commix()", 2, locals()
        return list_of_results
    
    def convoluteForArbitrarySpecificity(centremes, current_code=current_code):
        print "convoluteForArbitrarySpecificity ..." #DEBUG PRINT
        
        counta, bigmin, bigmax = convoluteForSpecificity(centremes, returnScores=True)
        scaled_up_target = scale(float(current_code), mn=0.0, mx=9.0, a=bigmin, b=bigmax)
        print "commix()", 3, locals()

        results = {}
        for item in counta.items():
            (rating, name), count = item
            diff_score = abs(rating - scaled_up_target)
            if diff_score < 30:
                try:
                    rank = ((rating*2.0) + count) / abs(rating - scaled_up_target) * 2.0
                except: #ZeroDivisionError
                    rank = (rating*2.0) + count * 4.0 #exactly the target: bonus.
                try:
                    results[rank].append(name)
                except KeyError:
                    results[rank] = [name]
            #else:
            #    print "Dropping item that's not in range really: ", name
        list_of_results = results.items()
        list_of_results.sort()
        print "commix()", 4, locals()
        return list_of_results
    
    def convoluteForOverlap(centremes):
        """A simplification of the above. Uses counter to count how many times a given lemmas comes up."""
        print "ConvoluteForOverlap" #DEBUG PRINT
        synsets = []
        lemmas = []
        for centreme in centremes:
            print "commix()", "!?!a", locals()
            synsets.extend( explode( wn.synsets(centreme) ) )
            print "commix()", "!?!b", locals()
        for synset in synsets:
            print "commix()", "!?!c", locals()
            lemmas.extend( [x.name.lower() for x in synset.lemmas] )
            print "commix()", "!?!d", locals()
        #overlap = list(intersection(zones[0], *zones))
        #random.shuffle(overlap)
        print "commix()", 5, locals()
        counta = Counter(lemmas)
        results = {}
        for item in counta.items():
            name, count = item
            rank = (count-1)*5 #subtract 1 so that 1-counts are of rank 0 (no bonus) then scale it up some to be similar proportions to HighestSpecificity method
            try:
                results[rank].append(name)
            except KeyError:
                results[rank] = [name]
        list_of_results = results.items()
        list_of_results.sort()
        print "commix()", 6, locals()
        return list_of_results

    
    try:
        if RNG( current_code*10 ): #assuming code is between 0 and 9 ...
            if current_code<=7:
                results = convoluteForArbitrarySpecificity(centremes)
            else:
                results = convoluteForHighestSpecificity(centremes) 
        else:
            if RNG(75):
                results = convoluteForOverlap(centremes)
            if RNG(25):
                for centreme in centremes:
                    blah = fetch_names( wn.synsets(centreme) )
                    blah = list(set( blah ) )
                    results.append( (50, blah) )
    
        #always put the originals in
        zzz = 28 + (10*current_code)
        results.append( ( zzz, centremes) )
        print "commix()", 'zzz', locals(), traceback.print_exc()
    except Exception as e:
        print "commix()", 'postszzz', locals(), traceback.print_exc()
        try:
            results.append( (10, centremes[0] ) )
        except Exception as e2:
            print "commix()", 'postszzzz', locals(), traceback.print_exc()
            pass

    print " Results ... ", results
    return results


def amoebicSplit(emes):
    #imperfectly split in half
    z = list( chunks(emes, len(emes)/2) )
    #then, if you have a "remainder" (i.e. it splits into three lists: two lists of length 6 but w a remainder-list with a length of 2
    #it pins the remainder-list onto the second, in order to avoid remainder-lists of length 1. also b/c that bridge will be crossed when blah blah blah
    if len(z[-1])<len(z[0]):
        z[1] += z.pop(-1)
    return z[0], z[1]

def divineMode(influence=None, current_code=5, mode_key="1122331233"):
    #placeholder: consider the CODE in conjunction with the WEIGHTS (from Options). Like map digits to modes based on weights.
    if not influence:
        y = int(mode_key[current_code])
        if y==3:
            return random.randint(1, 2)
        else:
            return y
    else:
        #placeholder for later -- when [*jajaja] spells can be cast by users
        return influence

def vagueness(lemma, whitelist=set([]), harshness=1):
    """Takes a word and a whitelist. Returns a demotion amount. If on whitelist, no demotion. Otherwise, demote according to membership in vague-lists.'"""
    demotion = 0
    if lemma not in whitelist:
        if lemma in FIRST_DEGREE_VAGUES: demotion = 15
        elif lemma in SECOND_DEGREE_VAGUES: demotion = 9
        elif lemma in THIRD_DEGREE_VAGUES: demotion = 8
        elif lemma in FOURTH_DEGREE_VAGUES: demotion = 7
        elif lemma in FIFTH_DEGREE_VAGUES: demotion = 5
        elif lemma in SIXTH_DEGREE_VAGUES: demotion = 3
    return demotion * harshness

def jibeWithPattern(mode, chainlink, all_possibilities, current_code=5, whitelist=set([])):
    """Using a single reference point from the poem, go through all the possible centremes tallying up similarities, then return the best.
    In other words this is a RE-SCORER. In its first incarnation it ignores previous scores, except that it randomly drops some of the lowest scores.
    (The scores I am referring to are the ones from commix)."""
    print "jibeWithPattern() -- CHAINLINK", chainlink
    chainlink = chainlink.lower()
    similarities = []
    done = 0
    todo = 25 + random.randint(current_code, (current_code+1)*4)
    #print "Mode: ", mode, ':::::::: Code: ', current_code #DEBUG PRINT
    #print 'Jibe with pattern todo # is', todo
    print "jibeWithPattern() -- All possibilities --", all_possibilities
    if mode==1: #MELOPOEIA
        reference = pronounce(chainlink)        
        while done<todo:
            try:
                this = all_possibilities.pop() 
                this_rank = this[0] #each item in all_possibilities is (common rank, [batch_of_items_w_this_rank])
                this_batch = this[1]
                rank_bonus = float(this_rank) * float((current_code+1.0)/10.0) #<-- testing ... incorporation of rank.
                #                                     current_code scales rank from 10% to 100% influence.
                
                #DEBUG PRINTS
                print "this_batch:", this_batch
                print "this_rank:", this_rank, "yielding a rank bonus of", rank_bonus
                
                for possibility in this_batch:
                    w = pronounce(possibility)
                    possibility = possibility.lower()
                    demotion = vagueness(possibility, whitelist, (current_code/7 + 1)) #currentcode/7+1 yields harshness of 2 for codes 7, 8, 9.
                    #                                                                    harsher vagueness demotions for higher specificity demanded.
                    if w and done<todo:
                        #The following combination of scoring methods is weird. It is not statistically optimal (what would be?)
                        #However after a day of testing & tinkering I've found that I am frequently delighted by the results
                        #    made possible by the method below. And it's super fast, despite being convoluted.
                        x = 1 + (1.5 * combo_ratio(reference[0], w[0]) ) #nysiis
                        y = 1 + combo_ratio(reference[1], w[1]) #double metaphone
                        z = 1 + combo_ratio(reference[2], w[2]) #soundex
                        a = 1 + (0.75 * combo_ratio(chainlink, possibility) ) #lettering
                        combined_score = ((x*z+y)*(x+y*z)) * (a*2-1.25) + (y*2-1) + rank_bonus - demotion
                        if chainlink==possibility:
                            # its the same word, which seems kind of boring.
                            # thus such a score is demoted by a random amount.
                            # the demotion is more severe for melo- than for phano-, for a couple reasons,
                            print "Same word! Scale down a pretty big amount." #DEBUG PRINT
                            combined_score = combined_score * random.random() - 25
                        similarity = combined_score, possibility
                        similarities.append(similarity)
                        done += 1
            except IndexError, e:
                print 'jibe>melo>IndexError', e
                break
            except TypeError, e:
                print 'jibe>melo>TypeError', e
                break
    elif mode==2: #PHANOPOEIA            
        while done<todo:
            try:
                this = all_possibilities.pop() 
                this_rank = this[0] #each item in all_possibilities is (common rank, [batch_of_items_w_this_rank])
                this_batch = this[1]
                rank_bonus = float(this_rank) * float((current_code+1.0)/10.0)
                
                #DEBUG PRINTS
                print "this_batch:", this_batch
                print "this_rank:", this_rank, "yielding a rank bonus of", rank_bonus
                
                for possibility in this_batch:
                    if done<todo:
                        possibility = possibility.lower()
                        demotion = vagueness(possibility, whitelist, (current_code/7 + 1)) #currentcode/7+1 yields harshness of 2 for codes 7, 8, 9.
                        #                                                                    harsher vagueness demotions for higher specificity demanded.
                        score = Levenshtein.ratio(chainlink, possibility)*300 + rank_bonus - demotion #<--scaled by 300 to align it with MELO.
                        if chainlink==possibility:
                            # its the same word, which seems kind of boring.
                            # thus such a score is demoted by a random amount.
                            print "Same word! Scale down somewhat." #DEBUG PRINT
                            score = score - 20
                        similarity = score, possibility
                        similarities.append(similarity)
                        done += 1
            except IndexError, e:
                print 'jibe>phano>IndexError', e
                print traceback.format_exc()
                break
            except TypeError, e:
                print 'jibe>phano>TypeError', e
                print traceback.format_exc()
                break
    #this is the wrench to throw in to test your results ...
    #similarities.sort()
    #breakkakajfj
    
    print "Comparisons done:", done #DEBUG PRINT
    
    try:
        #error-catching for if you wind up with an empty similarities list somehow
        similarities.sort()
        if similarities[-1][1]> 5: #minimum for satsifactory
            print "Similarities (sorted):", similarities #DEBUG PRINT
            #this generator & iterator find the first result that isn't in PROBLEM_LEMMA.
            def preened(data):
                print 'preened() data =', data
                for similarity, word in reversed(data):
                    print 'preened() word =', word
                    if word not in PROBLEM_LEMMAS: #(score, word-itself)
                        yield word
                    else:
                        if word in whitelist:
                            yield word
                        else:
                            print 'Dropped PROBLEM_LEMMA:', word #DEBUG PRINT
                            yield False
            print 'f'
            print "jibeWithPattern() -- similarities =", similarities
            for x in preened(similarities):
                print 'fuu'
                if x:
                    print "x =", x
                    return x
                print "x =", x
            #def compute(similarities):
            #    most_similar = similarities[n]
            #    tentative_winner = most_similar[1] #(score, word-itself)
            #    if
        else:
            print 'fuucksdksk'
            return False
    except Exception, e:
        print 'Exception ............', e #DEBUG PRINT
        return False

def selectCentreme(mode, all_possibilities, basepickle, melopickle, phanopickle, current_code=5, whitelist=set([])):
    """
    Selects a centreme according to some method of patterning.
    all_possibilities - must be list of possibly-next words/centremes in readable format
    basepickle - all the content-words (plus some other "important" words) in a list, in order of appearance in the poem...
    melopickle - list of lists of ... (etc) for the poem; chains ....
    phanopickle - sameish
    """
    centreme = None
    
    #mode selektorator##################################
    def modeStuff(mode, melopickle, phanopickle):
        total_recall = None
        active_memory_depth = None
        pickle = None
        if mode==2: #PHANOPOEIA: "the suggesting of visuality" -- lettering and so forth
            pickle = phanopickle
            total_recall = RNG(26 - current_code)
            active_memory_depth = 3 + random.randint(0, 4)
        if mode==1 or not pickle: #MELOPOEIA: "the suggesting of musicality and rhythm" -- sound
            pickle = melopickle
            total_recall = RNG(70 - (current_code*2))
            active_memory_depth = random.randint(3, (current_code/2)+5)
        return pickle, total_recall, active_memory_depth
    pickle, total_recall, active_memory_depth = modeStuff(mode, melopickle, phanopickle)
    
    #fork: minding total_recall flag, pick chainlink####
    chain, chain_index, active_memory = None, None, None
    pickle_length = len(pickle) #don't ask ...
    if total_recall:
        #chain selection####################################
        #REVISION: NOW, if there's total_recall, it's picking FROM A CHAIN, _NOT_ from basepickle.
        chain_index = random.randint(0, pickle_length-1) #this line and the next are like random.choice() except I need the index number returned explicitly
        print 'selectCentrme', 1
        chain = pickle[chain_index]
        try:
            chainlink = random.choice(chain)
            print 'selectCentreme', 2, chainlink, chain
        except: #<-- special case: first turn ever, there's nothing to refer back to ... so pick from one of the current possibilities why not.
            try: 
                possibility_batch = random.choice(all_possibilities)
                print 'selectCentreme', 3, possibility_batch, all_possibilities
                chainlink = random.choice( possibility_batch[1] )
                print 'selectCentreme', 3, chainlink
            except: #special error case that shouldn't ever happen...
                chainlink = 'phantom'
    else:
        #turn phanopickle into short-term memory############
        #    short-term memory is a list of n most-recent centremes, so that patterns aren't long-term sometimes. especially important with phano.
        try:
            active_memory = basepickle[-active_memory_depth:]
            print active_memory
            print 'selectCentreme', 4, active_memory
        except IndexError:
            active_memory = basepickle
        try:
            temp = 'yalp'
            chainlink = random.choice(active_memory)
            print 'selectCentreme', 5, active_memory
        except: #<-- special case: first turn ever, there's nothing to refer back to ... so pick from one of the current possibilities why not.
            try: 
                possibility_batch = random.choice(all_possibilities)
                print 'selectCentreme', 6, possibility_batch, all_possibilities
                chainlink = random.choice( possibility_batch[1] )
            except: #special error case that shouldn't ever happen...
                chainlink = 'phantom'
    #DEBUG PRINTS
    print 'TOTAL RECALL {', total_recall, '}'
    print 'Chainlink:', chainlink
    print 'Chain:', chain
    print 'Active memory:', active_memory
    print 'Pickle (either phano or melo):', pickle
    
    if chainlink=="entity": #<-- i really hate when that happens. (it's a like the "root" noun word or something)
        chainlink = 'phantom'

    print 'selectCentreme', locals()
    
    #jibeWithPattern####################################
    chosen = jibeWithPattern(mode, chainlink, all_possibilities, current_code, whitelist)
    print "chosen = ", chosen
    
    #placeholder: self-assessment#######################
    if not chosen:
        """
        #swap modes
        if mode==1: mode==2
        elif mode==2: mode==1
        #try again
        pickle, total_recall, active_memory_depth = modeStuff(mode, melopickle, phanopickle)
        chainlink = selectChainlink(total_recall, pickle, all_possibilities)
        chosen = jibeWithPattern(mode, chainlink, all_possibilities, current_code, whitelist)
        if not chosen: #once again nothing worked / was satisfactory, select a random one
            #this section is BROKEN right now ... FIX IT SOMETIME SOON ...
            possibility_batch = random.choice(all_possibilities)
            chosen = random.choice( possibility_batch[1] )
        """
        possibility_batch = random.choice(all_possibilities)
        chosen = random.choice( possibility_batch[1] )
        
    #turn chosen into centreme#########################
    chosen = chosen.lower() #for some reason things keep turning all-caps, this should fix that ...
    centreme = Eme(chosen.replace("_", " "))


    print "centreme, chosen = ", centreme, chosen

    #pickle updates####################################
    if total_recall:
        #if total_recall is on, we're working with a pre-existing chain; this new centreme needs to be appended to that chain.
        #i repeat, we're working on an existing chain; append this pronunciation to the list at that index.
        pickle[chain_index].append( chosen.lower() )
    else:
        #if not total_recall, we're working off of active_memory (which is itself a portion of the basepickle, not the other two ...)
        #that means we're not working off of a chain -- and that means we're starting a new one.
        #thus we append a new list to the pickle, with this centreme as inaugural item.
        if pickle_length>1:
            pickle.append( [chosen.lower()] )
        else:
            pickle[0] = [chosen.lower()]
    #swap pickle back to its appropriate place so it gets saved ..
    if mode==1: melopickle = pickle
    elif mode==2: phanopickle = pickle
    #then add the result into into the total words pickle ...
    basepickle.append(chosen.lower()) #lowering shit still ...

    print "locals()", locals()
    
    #done##############################################
    return centreme, basepickle, melopickle, phanopickle
    
def cookCentremes(mode, centremes, basepickle, melopickle, phanopickle, current_code=5):
    #favor coincidences / else: normal#################
    #believe it or not, this counts unique-considering-user-origin centremes, so that we can push through any *cross-computer* coincidences
    #try:
    #    most_coincidental, how_coincidental = Counter([y[0] for y in set([(str(x), x.user) for x in centremes])]).most_common(1)[0]
    #except IndexError: #sometimes there seems to be an empty list produced (and thus an IndexError). let's just say "fuck it" in these cases.
    #    how_coincidental = -1
    #how_coincidental==1 means the highest coincidence level was 1, which is singular occurence -- NOT coincidence...
    #if how_coincidental>1:
    #    centreme = most_coincidental
    #else:
        #overlap = commix(centremes)
        #if overlap:
        #    #possibilities = overlap+centremes
        #    centreme, melopickle, phanopickle = selectCentreme(mode, possibilities, melopickle, phanopickle)
        #else:
        #    centreme = None
    results_of_commix = commix(centremes, current_code=current_code)
    whitelist = set([x.lower() for x in centremes])
    print "got to refpoint QQQa", locals()
    centreme, basepickle, melopickle, phanopickle = selectCentreme(mode, results_of_commix, basepickle, melopickle, phanopickle, current_code=current_code, whitelist=whitelist)
    print "got to refpoint QQQb", locals()
    return centreme, basepickle, melopickle, phanopickle

def cookCentremesThoroughly(mode, centremes, basepickle, melopickle, phanopickle, current_code=5):
    """Sort of a wrapper for cookCentremes that has a convoluted way of handling problematic cases,
    one that I can't confirm (yet) as being functional ..."""
    if centremes:
        centreme, basepickle, melopickle, phanopickle  = cookCentremes(mode, centremes, basepickle, melopickle, phanopickle, current_code)
        print "got to refpoint XYZ"
        if not centreme: #<-- cooked[0] will be a chosen centreme or it will be False/None
            one, two = amoebicSplit(centremes)
            new_results = [ cookCentremes(mode, one, basepickle, melopickle, phanopickle), cookCentremes(mode, two, basepickle, melopickle, phanopickle) ]
            if new_results[0]==None and new_results[1]==None:
                cooked = selectCentreme(mode, centremes, basepickle, melopickle, phanopickle)
            #if we wound up with only result1 OR result2, use that ONE as the centreme.
            elif new_results[0]==None:
                cooked = new_results[1]
            elif new_results[1]==None:
                cooked = new_results[0]
            #this else-case means that we got TWO results from the split, so now we select between them.
            else:
                cooked = selectCentreme(mode, new_results, basepickle, melopickle, phanopickle)
            print "got to refpoint nocentrmewhatthefuck"
        else:
            cooked = centreme
        print "got to refpoint shouldreturnrightafterthis"
        return cooked, basepickle, melopickle, phanopickle
    else:
        raise "Yikes ... no centremes"



def cookAntemes(antemes):
    """Averages the antemes. Note that there is 1 anteme per user's utterance; one "anteme" (singular!) is a list of emes (tokens)."""
    if antemes:
        #initialize
        j = 0
        padded_antemes = []
        number_of_antemes = len(antemes)
        #Pad & trim
        antemes.reverse()
        for anteme in antemes:
            i = 0
            x = [-1]*X_EME_LENGTH
            try:
                while i<X_EME_LENGTH:
                    if not anteme[i].lower() in set(['s', 'n', 'e']):
                        x[i] = anteme[i]
                    i += 1
            except:
                pass
            padded_antemes.append(x)
        antemes.reverse()
        #iterate over the stuff
        x = []
        while j<X_EME_LENGTH:
            try:
                sum = 0.0
                pool = []
                for anteme in padded_antemes:
                    try: #the "fake-null" -1 's are integers and will add fine
                        sum += anteme[j] # i.e. this is SUPPOSED to be weird
                    except TypeError: #but if they're emes, we'll get a TypeError.
                        try: #if they're scored punctuation, this will work ...
                            sum += anteme[j].punc_value
                            pool.extend( anteme[j].punc_pool) # get its punc_pool too
                        except AttributeError: #if it's a stopword for example ...
                            sum += 99 #<-- ha
                            pool.append( anteme[j] )
                value = float(sum/number_of_antemes)
                if value > -1.0:
                    if value > 9.0: #some non-punctuational Eme has occured at this position, so strip all punctuational items ...
                        try:
                            for each in pool:
                                if not each[0].isalnum(): #sort of hacky but it works. all "words" and numbers would have an letter or number as their first character.
                                    pool.remove(each)
                        except IndexError, e:
                            print "index error at location fuck", e
                    else:
                        rounded = round(value)
                        try:
                            pool.extend( PUNC_TRANS[rounded] )
                        except KeyError:
                            pass
                    result = random.choice(pool)
                    if result: x.append( result )
                    
                j += 1
            #if RNG(1): #haha 1% chance of shuffling the results ...
            #    random.shuffle(x)
            except IndexError as e:
                print "IndexError ...", e
                return []
        return x
    else:
        return []

def cookPostemes(postemes):
    """Averages the postemes. Note that there is 1 posteme per user's utterance; one "posteme" (singular!) is a list of emes (tokens)."""
    if postemes:
        #initialize
        j = 0
        EOS_seen = False
        EOS_inserted = False
        EOS_bank = []
        padded_postemes = []
        number_of_postemes = len(postemes)
        #Pad & trim
        for posteme in postemes:
            i = 0
            x = [-1]*X_EME_LENGTH
            try:
                while i<X_EME_LENGTH:
                    if not posteme[i].lower() in set(['s', 'n', 'e']):
                        x[i] = posteme[i]
                    i += 1
            except:
                pass
            padded_postemes.append(x)
        #print "padded_postemes:", padded_postemes
        #iterate over the stuff
        x = []
        while j<X_EME_LENGTH:
            try:
                sum = 0.0
                pool = []
                for posteme in padded_postemes:
                    #print "this is a POSTEME --", posteme
                    try: #the "fake-null" -1 's are integers and will add fine
                        sum += posteme[j] # i.e. this is SUPPOSED to be weird
                        #print "WAS A NULL"
                    except TypeError: #but if they're emes, we'll get a TypeError.
                        try: #if they're scored punctuation, this will work ...
                            sum += posteme[j].punc_value
                            #print "Workin on ", posteme[j], " // ", posteme[j].punc_value, "..."
                            #print "Its punc_pool: ", posteme[j].punc_pool
                            #print "Just added", posteme[j].punc_value, "to sum"
                            pool.extend( posteme[j].punc_pool) # get its punc_pool too
                            if posteme[j].punc_value >= 6.0:
                                EOS_seen = True
                                EOS_bank.append( posteme[j] )
                        except AttributeError: #if it's a stopword for example ...
                            sum += 99 #<-- ha
                            pool.append( posteme[j] )
                value = float(sum/number_of_postemes)
                #print "value: ", value
                if value > -0.1:
                    if value > 9.0: #some non-punctuational Eme has occured at this position, so strip all punctuational items ...
                        try:
                            for each in pool:
                                if not each[0].isalnum(): #sort of hacky but it works. all "words" and numbers would have an letter or number as their first character.
                                    pool.remove(each)
                        except IndexError, e:
                            print "index error at location fuck", e
                    else:
                        rounded = round(value)
                        try:
                            pool.extend( PUNC_TRANS[rounded] )
                        except KeyError:
                            pass
                    result = random.choice(pool)
                    #print "Pool: ", pool
                    if result:
                        x.append( result )
                        if str(result)[0] in ",.;!?":
                            EOS_inserted = True

                j += 1 #I CAN'T BELIEVE THIS IS WHAT HAS BEEN MAKING THIS LAG. dlkfja
                
                #print "j: ", j
                #print "EOS_bank", EOS_bank
                #print "EOS_seen", EOS_seen
                #print "EOS_inserted", EOS_inserted
                
                #the following should only happen at the very end:
                if j>=X_EME_LENGTH:
                    #print "J>=X_EME_LENGTH"
                    if EOS_seen and not EOS_inserted:
                        try:
                            yyy = random.choice(EOS_bank)
                            
                        except IndexError:
                            yyy = "."
                        x.append(yyy)
            except IndexError, e:
                print "IndexError ...", e
                return []
        return x
    else:
        return []

"""
def addUtterance(poem, input, user):
    #validate the utterance
    utterance_cucumber = UtteranceCucumber(input, user.id)
    if utterance_cucumber.valid:
        #get latest cluster
        c = Cluster.objects.filter(poem=poem.pk).latest() # <-- probably not optimized!!
        if c.crushstate>0:
            g = GhostUtterance(poem=poem, text=utterance_cucumber.text, pickle=utterance_cucumber, user=user)
            g.save()
        else:
            #bind utterance to cluster
            u = Utterance(text=utterance_cucumber.text, pickle=utterance_cucumber, user=user)
            try:
                lookup = Utterance.objects.get(clusters=c, text=utterance_cucumber.text)
            except ObjectDoesNotExist:
                u.save()
                intermediary = UtteranceMembership.objects.create(utterance=u, cluster=c)
                intermediary.save()
                poem.stats.utterances = c.utterance_set.count()
                if poem.stats.utterances >= poem.stats.max_utterances:
                    #from tasks import cookCluster # DYNAMICALLY IMPORT ITEM FROM TASKS.
                    task = cookCluster.delay(poem.id) #queue the task of crushin
                    poem.stats.loading = ( poem.stats.max_utterances*2 - 2) #this might need updating, later on ...
                poem.stats.save()
    #return utterance, which includes any messages that need to be sent to the user ...
    return utterance_cucumber
"""





