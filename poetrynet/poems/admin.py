from django.contrib import admin
from poetrynet.poems.models import Utterance, GhostUtterance, Cluster, Poem, PoemOptions, PoemStats, Ping, UtteranceMembership

admin.site.register(Poem)
admin.site.register(PoemOptions)
admin.site.register(PoemStats)
admin.site.register(Ping)
admin.site.register(UtteranceMembership)

class UtteranceMembershipInline(admin.TabularInline):
    model = Utterance.clusters.through

class ClusterAdmin(admin.ModelAdmin):
    inlines = [
        UtteranceMembershipInline,
    ]

class UtteranceAdmin(admin.ModelAdmin):
    inlines = [
        UtteranceMembershipInline,
    ]
    exclude = ('clusters',)



admin.site.register(Cluster, ClusterAdmin)
admin.site.register(Utterance, UtteranceAdmin)
admin.site.register(GhostUtterance)