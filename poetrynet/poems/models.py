from django.db import models
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404
import random

#import pickle_fields, pickle_tests
from picklefield.fields import PickledObjectField

#FLOODING_TOLERANCE_CHOICES = (
#    (0, '0'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, '6'), (7, '7'), (8, '8'), (9, '9'),
#)

#PUNCTUATION_WEIGHT_CHOICES = (
#    (0, '0'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, '6'), (7, '7'), (8, '8'), (9, '9'),
#)


class PoemOptions(models.Model):
    """
    The custom options of a poem, decoupled from the poem itself because all of that data isn't needed at every step.
    Also, the default optionset is id=1.
    """

    ####COMMON OPTIONS (SOCIAL OPTIONS)####
    max_simultaneous_authors = models.IntegerField("Max simultaneous authors",help_text="Maximum users that can participate in the creation of this poem at once. 0 means no limit!", default=0)
    no_user_list = models.BooleanField(help_text="Hide the list of authors until the poem is published for the first time.", default=False)
    #no_chatting = models.BooleanField(help_text="Disallow private messaging in this poem-session.", default=False)
    #not an option now but a possibile one -- switch for whether Client display shows "how many words till next crush" or somn.
    
    ####LESS COMMON OPTIONS (PARAMETERS)####
    #invite_only = models.BooleanField(help_text="Do users need to be invited to this poem?", default=False)
    #max_total_authors = models.IntegerField(help_text="Maximum users that can participate in the creation of this poem overall. 0 means no limit", default=0)
    #authorship_public = models.BooleanField("Author visibility", help_text="Display authorship to public?", default=True)
    max_length = models.IntegerField("Maximum length", help_text="Optional maximum length of poem/session (in outputs/rounds). 0 means no limit!", default=0)
    
    ####ADVANCED OPTIONS (MECHANICS)####
    #flooding_tolerance = models.IntegerField(help_text="Tolerance for input-floods from a given user. 0 is no tolerance, 9 is unpatrolled", choices=FLOODING_TOLERANCE_CHOICES, default=5)
    #logopoeia_weight = models.IntegerField(help_text="Logopoeia % chance.", default="34")
    #phanopoeia_weight = models.IntegerField(help_text="Phanopoeia % chance.", default="33")
    #melopoeia_weight = models.IntegerField(help_text="Melopoeia % chance.", default="33")
    #punctuation_weight = models.IntegerField(help_text="How much of the inputted punctuation to preserve? 0 is none, 9 is as much as possible", choices=PUNCTUATION_WEIGHT_CHOICES, default=5)
    #related_poem = models.CharField("Related poem", help_text="Refer the algorithm back to another poem on this website? To utilize this option, type the title of that poem verbatim.", max_length=255, blank=True, null=True, default=" ")
    #no_meta = models.BooleanField("No meta characters", help_text="Disallow metacharacters like <w>, <italics>, etc.", default=False)
    #no_proper_nouns = models.BooleanField(help_text="Disallow proper nouns", default=False)
    #no_gibberish = models.BooleanField(help_text="Disallow gibberish", default=False)
    #freeze = models.BooleanField(help_text="Freeze poem after publishing?", default=False)
    #absorb_chatting = models.BooleanField(help_text="Experimental option -- whether to absorb private messages uttered in this room into the poem's corpus.", default=False)

class PoemStats(models.Model):
    
    ####ROBO-MANAGED FIELDS & VARIABLES####
    #max_utterances = models.IntegerField(help_text="Number of utterances in each cluster; this should get updated each time a user joins/leaves", default=2)
    utterances = models.IntegerField(help_text="...", default=0)
    
    views = models.IntegerField("Views", help_text="Views since first publishing.", default=0)
 
    active_authors = models.ManyToManyField(User, through='Ping', related_name="AC")
    loading = models.IntegerField("Metertag - Loading?", help_text="Yes/no", default=0)
    serving = models.BooleanField("Spinner-Cursor Boolean", help_text="While a cluster is being postprocessed a spinner is displayed, this is the flag for whether it is or not.", default=False)
    last_sentence = models.CharField(help_text="Stuff since last 'end of sentence marker', i.e. newline, period, caps, etc.", blank=True, null=True, default="~", max_length=256)
    last_benchmark = models.TextField(help_text="Can be: [.;%] meaning EOS; C (or J) meaning centreme (or adj/adv cent); or X|word for non-centreme words", default=".")
    clusters_computed = models.IntegerField(help_text="i.e. what round is this.", default=0)
    
    ####CODES & INTERNAL WORKINGS####
    code = models.CharField(help_text="A seed-code used throughout the algorithms. Must be 10 digits long.", max_length=10, default="0123456789")
    step = models.IntegerField(help_text="Step 0-9 in the code.", max_length=1, default=0)
    walking_forward = models.BooleanField(help_text="Are we moving right (True) or left (False) through the code?", default=True)
    mode_key = models.CharField(help_text="A 10-digit reference for WHAT MODE to use.", max_length=10, default="1122331233")
    #cleanup_flag = models.IntegerField(help_text="Incremented each time a cluster is cooked, so that certain things can be done every N cluster-cookings", default=0)

class Poem(models.Model):
    created_date = models.DateTimeField(help_text="When first created", auto_now_add=True)
    pub_date = models.DateTimeField("Date published", help_text="When first published", blank=True, null=True) #poems should not be listed in listings-for-reading until published.
    authors = models.ManyToManyField(User, related_name="AU")
    stats = models.ForeignKey(PoemStats)
    creator = models.ForeignKey(User, related_name="CR")
    
    ####ESSENTIAL OPTIONS####
    title = models.CharField(max_length=255, unique=True)
    welcome_message = models.TextField("Welcome message",
                                        default="Welcome.",
                                        help_text="Displayed to each user upon joining the poem-session. Not displayed with the published poem.")
    
    ####EXTRA OPTIONS####
    customized = models.BooleanField("Customization", help_text="Fine-tune the options of this poem-session?", default=False)
    options = models.ForeignKey(PoemOptions)
        
    #facade: textual representation of this poem. this is the last way this poem looked
    #when it was saved last (usually that means: the final or last appearance of the poem
    #from the performance that generated it). when a poem is shuffled again, that creates
    #an appearance for the client who has shuffled it; but they'd need to save a new Poem
    #from that, if they wanted to preserve that appearance (and that Poem object's 'facade'
    #would be, indeed, the textual representation at the time of their save.
    facade = models.TextField(help_text="Last textual representation of this poem -- (cheaply) HTML formatted, _NOT_ plaintext!!")
    html = models.TextField(help_text="HTML-ified-version")
    basepickle = PickledObjectField(help_text="Pickled data representing the poem's content-words (plus other important words) in order of appearance.")
    phanopickle = PickledObjectField(help_text="Pickled data representing the poem's chains made by lettristic functions.")
    melopickle = PickledObjectField(help_text="Pickled data representing the poem's chains made by melopoeiac functions.")
    
    def newCluster(self):
        """makes the first cluster for this poem."""
        try:
            c = Cluster(poem=self)
            c.save()
            return c
        except:
            try:
                c = Cluster(poem=self)
                c.save()
                return c
            except:
                return None
    
    def shuffle(self):
        clusters = Cluster.objects.filter(poem=self.pk)
        #not completed. this should be the "reshuffle" ability. reshuffles EVERYTHING. not relevant yet ..
        pass 
    
    def save(self, *args, **kwargs):
        #pre-super:
        firsttime=False
        if self.id is None:
            firsttime=True
        super(Poem, self).save(*args, **kwargs) #Call the "real" save() method ...
        if firsttime:
            self.newCluster()
        #post-super:
    
    def __unicode__(self):
        return unicode(self.title)

class Ping(models.Model):
    """Logs (and cleans up) records of who is pinging what poems..."""
    poemstats = models.ForeignKey(PoemStats)
    user = models.ForeignKey(User)
    date = models.DateTimeField("Date created.", auto_now_add=True) 
    
class Cluster(models.Model):
    """A collection of user inputs sent to the same poem at the same step of writing it."""
#    utterances = models.ManyToManyField(Usaving a tterance, through='UtteranceMembership')
    poem  = models.ForeignKey(Poem)
    #The field below, max_utterances, seems like it may in fact be deprecated.
    #max_utterances = models.IntegerField("Number of utterances-per-cluster at the time this cluster was CRUSHED. (For reference in later shuffles.)", default=7)
    facade = models.CharField("Last textual representation of this cluster", max_length=160, blank=True, null=True, default="")
    html = models.CharField("HTML version of facade", max_length=255, blank=True, null=True, default="")
    date = models.DateTimeField("Date created.", help_text="When cluster was born.", auto_now_add=True)
    crushstate = models.IntegerField( default=0)
    lock_date = models.DateTimeField("For locking.", help_text="When this cluster was locked.", null=True, blank=True, default=None)
    pickle = PickledObjectField(help_text="This is the pickled cluster, saved here when cookCluster has cooked it. pickle[0]=(anteme,centreme,posteme); pickle[1]={Cooked-but-not-served results string}",  null=True, blank=True, default=None)
    #one day you could make the clusters take note of the results of divineMode (phano/melo/logo), if the variation between shuffles is too radical.
    
    def __unicode__(self):
        return unicode(self.facade)
    
    class Meta:
        ordering = ["date"]
        get_latest_by = "date"

class Utterance(models.Model):
    """An instance of user input to a poem; I _believe_ these will be checked before saved."""
    #right now I'm thinking we don't need any reference to the user who said it or when it happened, actually.
    clusters = models.ManyToManyField(Cluster, through='UtteranceMembership')
    text = models.CharField(max_length=160) #do a test later to make sure tabs + linebreaks are getting encoded right
    date = models.DateTimeField(auto_now_add=True)
    pickle = PickledObjectField(help_text="This is a pickled object: a list of word/punctuation objects, python style ...")
    user = models.ForeignKey(User)

    def save(self, *args, **kwargs):
        #pre-super:
        #dostuff##
        super(Utterance, self).save(*args, **kwargs) #Call the "real" save() method ...
        #post-super:
        #dostuff##
        return self
    
    def __str__(self):
        return self.text
    def __unicode__(self):
        return self.text    
    
    class Meta:
        ordering = ["date"]
    
class UtteranceMembership(models.Model):
    """Clusters have many Utterances; UtteranceMembership tracks this"""
    utterance = models.ForeignKey(Utterance)
    cluster = models.ForeignKey(Cluster)
    
class GhostUtterance(models.Model):
    """An utterance which was having trouble getting added to a cluster ..."""
    #right now I'm thinking we don't need any reference to the user who said it or when it happened, actually.
    text = models.CharField(max_length=160) #do a test later to make sure tabs + linebreaks are getting encoded right
    poem  = models.ForeignKey(Poem)
    pickle = PickledObjectField(help_text="This is a pickled object: a list of word/punctuation objects, python style ...")
    user = models.ForeignKey(User)
    
    def __str__(self):
        return self.text
    def __unicode__(self):
        return self.text    