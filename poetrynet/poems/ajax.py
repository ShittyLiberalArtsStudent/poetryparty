from dajax.core import Dajax
from dajaxice.core import dajaxice_functions

from django.core.exceptions import ObjectDoesNotExist

#from xyz import addUtterance
from poetrynet.poems.models import Cluster, GhostUtterance, Utterance, UtteranceMembership, Poem
from poetrynet.poems.utils import UtteranceCucumber, flooding, whitespace2html


def addUtterance(poem, input, user):
    #validate the utterance
    utterance_cucumber = UtteranceCucumber(input, user.id)
    if utterance_cucumber.valid:
        #get latest cluster
        c = Cluster.objects.filter(poem=poem.pk).latest() # <-- probably not optimized!!
        if c.crushstate==1:
            g = GhostUtterance(poem=poem, text=utterance_cucumber.text, pickle=utterance_cucumber, user=user)
            g.save()
            if poem.stats.utterances >= 2:
                from poetrynet.poems.tasks import cookCluster # DYNAMICALLY IMPORT ITEM FROM TASKS.
                task = cookCluster.delay(poem.id) #queue the task of crushin
                poem.stats.loading = 1 #this might need updating, later on ...       
            else:
                poem.stats.utterances += 1
            poem.stats.save()
            poem.save()         
        else:
            #catch cooked-but-uninserted info
            if c.crushstate==4:
               if c.facade not in poem.facade:
                   c.crushstate = 3
                   c.save()
                   from poetrynet.poems.tasks import insertText
                   poem.stats.serving = 1
                   task = insertText.delay(poem.id, c.id)
            #force-make-new-cluster-if-issue
            if c.crushstate>=3:
                newC = poem.newCluster()
                if not newC:
                    try:
                        newC = poem.newCluster()
                    except:
                        try:
                            newC = poem.newCluster()
                        except:
                            pass
                if newC:
                    c = newC
            #bind utterance to cluster
            u = Utterance(text=utterance_cucumber.text, pickle=utterance_cucumber, user=user)
            try:
                lookup = Utterance.objects.get(clusters=c, text=utterance_cucumber.text)
            except ObjectDoesNotExist:
                u.save()
                intermediary = UtteranceMembership.objects.create(utterance=u, cluster=c)
                intermediary.save()
                poem.stats.utterances = c.utterance_set.count()
                if poem.stats.utterances >= 2:
                    from poetrynet.poems.tasks import cookCluster # DYNAMICALLY IMPORT ITEM FROM TASKS.
                    task = cookCluster.delay(poem.id) #queue the task of crushin
                    poem.stats.loading = 1 #this might need updating, later on ...
                poem.stats.save()
    #return utterance, which includes any messages that need to be sent to the user ...
    return utterance_cucumber


def senditin(request, input, poem_id):
    dajax = Dajax()
    user = request.user
    message = ''
    debug = []
    effect = None
    
    message = "Test."

    if input:
        length = len( str(input) )
        if length>0:
            if length<1000:
                if not flooding(user):
                    #get the poem
                    p = Poem.objects.get(id=poem_id) #certainly not optimized!
                    
                    if not p.pub_date:
                        #attempt to enter the utterance via addUtterance(poem, utterance) in utils.py
                        try:
                            utterance_cucumber = addUtterance(p, input, user) #Note!: u is an UtteranceCucumber instance, not a django data object
                        except Exception, e:
                            debug.append(str(e))
                        
                        #roll out the ajax things that should follow       
                        try:
                            message = utterance_cucumber.message
                            if not message:
                                message = "Last submission: " + whitespace2html(utterance_cucumber.text)
                            #dajax.assign('#windeyetext','innerHTML',p.facade)
                            dajax.assign('#talkbox','value', utterance_cucumber.remainder)

                            print message
                            
                            #if not u.valid:
                            #    dajax.script('$( "#result" ).effect( highlight )')
                        except Exception, e:
                            debug.append(str(e))
                        
                    else:
                        message="Poem has been published and is no longer writable. Redirecting ..."
                        scr = """window.location = "/poems/view/%s/" """ % p.id
                        dajax.script(scr)
                        print message
                else:
                    message="Please slow down. Try again in a few moments."
                    #placeholder: dajax.script that removes that message after a couple seconds.
                    print message
            else:
                message="Inputs cannot be longer than 1000 characters, sorry!"
                print message
        else:
            message="Please try entering your input again. If this message persists for some reason, please modify your input slightly and try again -- this is a little bug that happens sometimes, apologies!"
            print message
    else:
        message="You didn't type anything."
        print message
    
    dajax.assign('#lastwords', 'innerHTML', message )
    if debug:
        dajax.assign('#lastwords', 'innerHTML', str(debug))
    return dajax.json()

dajaxice_functions.register(senditin)