from datetime import timedelta, datetime
import time
import random
import traceback

from celery.task import task, periodic_task # instead of celery.decorators
from celery.exceptions import SoftTimeLimitExceeded
from django.db import DatabaseError
from poetrynet.poems.models import Poem, Cluster, GhostUtterance, Utterance, UtteranceMembership, \
    Ping

from utils import whitespace2html, divineMode, cookAntemes, cookPostemes, cookCentremesThoroughly, LINE_LENGTH_LIMIT
from poetrynet.poems.aleatorics import RNG
from poetrynet.poems.editor import EOS



#note: add ignore_result=False to all tasks to "reduce resource usage"!

@periodic_task(run_every=timedelta(seconds=20), ignore_result=False)
def cleanUp():
    """ Counterpart to ping(). This clears out all Ping objects that were touched more than 20 seconds ago. """
    try:
        #Delete old pings######
        thirty_seconds_ago = datetime.now()-timedelta(seconds=20)
        Ping.objects.filter(date__lt=thirty_seconds_ago).delete()
        return "Deleted stale pings."
    except DatabaseError:
        time.sleep(1)
        return "Cleanup hit a database error and then Slept for a second"
    except:
        return "cleanUp hit an unexpected error."

#@periodic_task(run_every=timedelta(seconds=21), ignore_result=False)
#def updateMaxUtterances():
#    """Updates the MaxUtterances count for every active poem, every 21 seconds."""
#    try:
#        active_poems = PoemStats.objects.exclude(active_authors=None)
#        if active_poems:
#            for each in active_poems:
#                user_count = each.active_authors.count()
#                each.max_utterances = calculateMaxUtterances(user_count)
#                each.save()
#                return "Updated MaxUtterances on all active poems."
#        else:
#            return "No active poems"
#    except DatabaseError:
#        time.sleep(1)
#        return "Slept for a second"

@task()
def updateLastSentence(poem_id, ignore_result=False):
    try:
        #dynamic imports ...
        from poetrynet.poems.corpus_utils import fetchPunkt
        import re
        poem = Poem.objects.get(pk=poem_id)
        #plaintext = whitespace2plaintext(poem.facade)
        plaintext = poem.facade.replace('%', '. ')
        plaintext = plaintext.replace('\n', '. ')
        last_period_index = plaintext.find('.')
        if last_period_index:
            plaintext = plaintext[last_period_index+1:]
        p = fetchPunkt() #this function returns a sentence tokenizer
        sentences = p.tokenize(plaintext)
        last_sentence = sentences[-1]
        try:
            if re.search(last_sentence, r'[%s]+' % EOS):
                last_sentence = "~"
        except:
            last_sentence = ". "
        poem.stats.last_sentence = last_sentence
        poem.stats.save()
        poem.save()
        return "Updated last_sentence field."
    except DatabaseError:
        time.sleep(1)
        return "Slept for a second"
    except:
        try:
            poem = Poem.objects.get(pk=poem_id)
            poem.stats.last_sentence = ". "
            poem.stats.save()
            poem.save()
            return "updateLastSentence hit an unexpected error but was able to do soemthing arbitrary instead."
        except:
            return "updateLastSentence hit an unexpected error, and did nothing good at all."


@task(ignore_result=False)
def insertText(poem_id, cluster_id):
    try:
        #initialize
        poem = Poem.objects.get(pk=poem_id)
        cluster = Cluster.objects.get(pk=cluster_id)
        if cluster.crushstate < 4:
            #an extra clause to make sure spaces get added dammit
            try:
                if poem.facade[-1].isalnum() and cluster.facade[0].isalnum():
                    if RNG(95):
                        cluster.facade = " " + cluster.facade
                    else:
                        cluster.facade = ". " + cluster.facade
            except:
                pass
            #HTML STUFF
            html_pre = cluster.html # if an auto-inserted <br/> happened, this gets it
            yyy = whitespace2html(cluster.facade) #don't forget to make it html friendly
            #TOOLTIP STUFF#
            html_a = """<a class="tt" href="#">"""
            html_b = yyy
            html_c = """<span class="cl"><b>Inputs</b><br/>"""
            ellipsis = "" #ellipsis inside tooltip if necessary
            j = 0
            for input in [str(utterance.pickle.text) for utterance in cluster.utterance_set.all()]:
                html_c += input
                html_c += "<br/>"
                j += 1
                if j>4:
                    ellipsis = "..."
                    break
            html_c = html_c[:-5] + ellipsis + """</span></a>"""
            
            cluster.html = html_pre + html_a + html_b + html_c
            poem.html += cluster.html
            
            #PLAINTEXT STUFF
            poem.facade += cluster.facade
            
            ##########
            cluster.crushstate = 4 # inserted.
            cluster.save()
            poem.stats.serving = False
            poem.stats.save()
            poem.save()
            updateLastSentence.delay(poem_id)
            return "Text inserted."
        else:
            return "Already inserted?"
    except:
        return "insertText hit an unexpected error and did nothing good at all."

        
@task(ignore_result=False, soft_time_limit=60, time_limit=120)
def serveCluster(poem_id, cluster_id):
    """This does the grammar-normalization and stuff."""
    try:    
        #initialize
        poem = Poem.objects.get(pk=poem_id)
        cluster = Cluster.objects.get(pk=cluster_id)
        print 'yep'
        
        if cluster.crushstate < 3:
            #dynamic imports yah
            from poetrynet.poems.nodebox_utils import inflectAndEvaluate
            
            #some shuffling of data
            print cluster.pickle[2] # PRINT DEBUG
            stuff = cluster.pickle[0]
            capitalizings = cluster.pickle[1]
            anteme, centreme, posteme = stuff[0], stuff[1], stuff[2]
            print stuff, anteme, centreme, posteme
            print cluster.pickle
            print cluster.pickle[:]
            antecedent = poem.stats.last_sentence
            last_benchmark = poem.stats.last_benchmark
            
            served_cluster, final_bm = inflectAndEvaluate(last_benchmark, antecedent, anteme, centreme, posteme, capitalizings)

            print 1, locals()
            
            #clean up & save
            if "\n" in cluster.facade:
                qqq = str(served_cluster)
                if qqq[0]==" " and RNG(50):
                    cluster.facade += qqq[1:]
                else:
                    cluster.facade += qqq
            else:
                cluster.facade = str(served_cluster)
            cluster.crushstate = 3 #served
            cluster.save()
            #poem.facade += whitespace2html(served_cluster) #don't forget to make it html friendly

            print 2, locals()
            poem.stats.loading = 0
            poem.stats.last_benchmark = final_bm
            poem.stats.clusters_computed += 1
            #poem.stats.serving = False
            poem.stats.save()
            poem.save()
            print 3, locals()

            insertText.delay(poem_id, cluster_id)
            #updateLastSentence.delay(poem_id)
            print 4, locals()
            return "Cluster served!"
        else:
            print 5, locals()
            return "Already crushed?"
    except SoftTimeLimitExceeded:
        print 6, locals()
        return "Timelimit exceeded X__X"
    except DatabaseError:
        time.sleep(1)
        print 7, locals()
        return "Slept for a second"
    except Exception as e:
        print 8, locals()
        print e
        print traceback.format_exc()
        return "serveCluster hit an unexpected error, and did nothing good at all."

@task(ignore_result=False, soft_time_limit=60, time_limit=120)
def cookCluster(poem_id=0, cleanup=False):
    try:
        ##BOOT##
        #LATER UPDATE: make it so a poem_id is NOT specified in advance; and instead, it just cooks ALL unfinished clusters ... like figures out which poem they go to, itself...
        start_time = time.time() #PRINT DEBUG NEEDS
        poem = Poem.objects.get(pk=poem_id)
        abort = False
        if not cleanup:
            cluster = Cluster.objects.filter(poem=poem.pk).latest()
        else:
            try:
                cluster = Cluster.objects.filter(poem=poem.pk, crushstate__gt=0, crushstate__lt=2).latest()
            except Exception, e:
                print "EXCEPTION:", e
                abort = True
        if not abort:
            if cluster.utterance_set.all(): #<-- this is false if the cluster is actually a false / empty cluster ..
                if cluster.crushstate < 1:
                    cluster.crushstate = 1
                    cluster.lock_date = datetime.now()
                    cluster.save()
                    
                    ##################
                    ##PROCESSING######
                    ##################
                    ##CODE, MODE##
                    current_code = int( poem.stats.code[poem.stats.step] )
                    mode_key = poem.stats.mode_key
                    mode = divineMode(influence=None, current_code=current_code, mode_key=mode_key)
                    
                    ##ANTEMES##
                    antemes = [utterance.pickle.anteme for utterance in cluster.utterance_set.all()]
                    anteme = cookAntemes(antemes)
                    results = "".join(anteme)
                    
                    ##CENTREMES##
                    centremes = [utterance.pickle.centreme for utterance in cluster.utterance_set.all()]
                    centreme, poem.basepickle, poem.melopickle, poem.phanopickle = cookCentremesThoroughly(mode, centremes, poem.basepickle, poem.melopickle, poem.phanopickle, current_code)
                    results += centreme
                    print "got to refpoint CENTREMES_DONE"
                    
                    ##POSTEMES##
                    postemes = [utterance.pickle.posteme for utterance in cluster.utterance_set.all()]
                    posteme =  cookPostemes(postemes)
                    results += "".join(posteme)
                    print "got to refpoint POSTEMES_DONE"
                    
                    ##CAPITALIZINGS##
                    capitalizings = []
                    capitalizings = [i for i in utterance.pickle.capitalizing for utterance in cluster.utterance_set.all()]
                    print "CAPITALIZINGS ", capitalizings
                    
                    ##################
                    ##POSTPROCESSING##
                    ##################
                    ##1. CONVERT TO PLAINTEXT FOR THESE FUNCTIONS' SAKE.
                    temp = poem.facade.replace('%', '\n')
                    
                    ##2. MAKE SURE LINES AUTOMATICALLY BREAK IF THEY ARE ABOUT TO EXCEED THE WINDOW WIDTH.
                    ##note: facade is HTML formatted already; thus for display-measurement, gotta change it to being plaintext.
                    t = temp + results
                    n = t.rfind('\n')
                    if n>-1:
                        t = t[n:]
                    #we're paying attention to the last LINE_LENGTH_LIMIT characters _SINCE_ the most recent newline (if one has happened).
                    last_x_chars = t[-LINE_LENGTH_LIMIT:]
                    if len(last_x_chars) >= LINE_LENGTH_LIMIT:
                        cluster.facade = "\n"
                        if RNG(30):
                            cluster.facade +="\n"
                        #cluster.html = "<br/>"
                    
                    ##3. PRETTIFY WHITESPACE AS NECESSARY.
                    #results = utils.normalizeWhitespace(results, preceding_character=plaintext[-1:])
                    
                    ##LASTLY. STEP FORWARD IN CODE##
                    ####1. determine next step based on current direction
                    if RNG(95 - current_code):
                        if poem.stats.walking_forward==True:
                            next_step = poem.stats.step + 1
                        else:
                            next_step = poem.stats.step - 1
                    else: #sometimes it jumps randomly.
                        next_step = random.randint(0, 9)
                    if next_step>9: #wrap. ignore direction because it's implied ...
                        next_step = 0
                    elif next_step<0:
                        next_step = 9
                    ####2.determine whether direction changes
                    if poem.stats.walking_forward==True: chance = 5
                    else: chance = 20
                    if RNG(chance): #if we change direction
                        if poem.stats.walking_forward==True: poem.stats.walking_forward==False
                        else: poem.stats.walking_forward==True
                    poem.stats.step = next_step #wow I forgot to do this, so was doing nothing
                    #TOTALLY WRONG: poem.stats.step = poem.stats.code[next_step]
                    print "step,", next_step
                    print "the code then,", poem.stats.code[next_step]
                
                    ##################
                    ##SAVE RESULTS####
                    ##################
                    cluster_id = cluster.id
                    cluster.pickle = ([anteme, centreme, posteme], capitalizings, "Pre-served results: %s" % str(results))
                    cluster.crushstate = 2 #and mark it as totally cooked/crushed
                    cluster.save() #then save it.
                    #don't inscribe the new cluster yet, that gets done in serveCluster
                    #instead, you will pass the cooked cluster to serveCluster. Fix whitespace first though.
                    newC = poem.newCluster()
                    if not newC:
                        try:
                            newC = poem.newCluster()
                        except:
                            try:
                                newC = poem.newCluster()
                            except:
                                pass
                    if newC:
                        poem.stats.utterances = 0
                        stragglers = GhostUtterance.objects.filter(poem=poem)[:3] #<-- NOTE it only takes 3
                        for straggler in stragglers:
                            print "HANDLING A STRAGGLER!"
                            u = Utterance(text=straggler.text, pickle=straggler.pickle, user=straggler.user)
                            u.save()
                            intermediary = UtteranceMembership.objects.create(utterance=u, cluster=newC)
                            intermediary.save()
                            poem.stats.utterances += 1
                            straggler.delete()
                    
                    #poem.stats.loading = False
                    poem.stats.serving = True
                    #cleanup thing
                    #if poem.stats.cleanup_flag > 3:
                    #    print "POEM CLEANUP FLAG > N, SO I PUT IN ANOTHER COOKCLUSTER REQUEST"
                    #    poem.stats.cleanup_flag = 0
                    #    cookCluster.delay(poem_id, cleanup=True)
                    #else:
                    #    poem.stats.cleanup_flag += 1
                    poem.stats.save()
                    poem.save()
                    
                    ## START POSTPROCESSING ##
                    serveCluster.delay(poem_id, cluster_id)
                    
                elif cluster.crushstate==1:
                    delta = datetime.now()-cluster.lock_date
                    #total_seconds() is gone in python 2.6 so this is maybe a workaround
                    time_since_lock = delta.days * 86400.0 + delta.seconds + delta.microseconds * 1e-6
                    print "TIME SINCE LOCK :: :: :: :: ", time_since_lock #DEBUG PRINT
                    if time_since_lock>5:
                        cluster.crushstate = 0
                        cluster.save()
                        results = "Cluster was hanging. New request put in."
                        try:
                            cookCluster.delay(poem_id)
                        except:
                            pass
                    else:
                        results = "This cluster is probably being processed right now, as it has been less than 7 seconds since it was locked."
                elif cluster.crushstate>=2:
                    time.sleep(2)
                    newC = poem.newCluster()
                    if not newC:
                        try:
                            newC = poem.newCluster()
                        except:
                            try:
                                newC = poem.newCluster()
                            except:
                                pass
                    if newC:
                        results = "That funky thing is happening with the cluster, so we made a new cluster."
                else:
                    results = "Cluster-crushing had already been initiated!"
            else:
                cluster.delete()
                results = "Invalid cluster: no utterances."
        else:
            results = "Attempted cleanup but there was nothing to clean. Aborted!"
        print "Time Elapsed:", time.time()-start_time
        #print results
        return "cookCluster results: %s" % results
    except SoftTimeLimitExceeded:
        return "Timelimit exceeded X__X"
    except DatabaseError:
        time.sleep(1)
        return "Slept for a second"
    except:
        return "cookCluster hit an unexpected error, and did nothing good at all."

    
