from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from django.core.urlresolvers import reverse
from django.http import Http404

#from django.contrib.sessions.models import Session
from django.contrib.auth.models import User

from datetime import datetime
import random
from poetrynet.poems.models import Poem, Ping, PoemOptions, PoemStats

from poetrynet.poems.project_utils import render_response

#############################################
####FAUX CGI (HANDCODED PROTOTYPE AJAX)######
#############################################
from poetrynet.poems.utils import whitespace2html, initialPing


def yours(request, clothe=True):
    user = request.user
    html = ""
    if not user.is_anonymous():
        your_poems = Poem.objects.filter(authors__pk=user.id).order_by('-created_date', '-pub_date', 'title')
        how_many_total = your_poems.count()
        if how_many_total > 0:
            if how_many_total > 10: h = True
            else: h = False
            html += "<ul>"
            for poem in your_poems[:10]:
                html += """<li><a href="/poems/view/%s">%s</a></li>""" % (str(poem.id), poem.title)
            if h:
                html += """<li><a href="/poems/by/%s">See all %s &raquo;</a></li>""" % (user.username, how_many_total)
            html += "</ul>"
        else:
            html="""<p>(You haven't participated in any poems <a href="/poems/create/">yet</a>.)</p>"""
    if clothe:
        return HttpResponse(html)
    else:
        return html

def userlist(request, id):
    poem = Poem.objects.get(pk=id)
    html = " "
    if poem.stats.active_authors.all():
        for x in poem.stats.active_authors.all():
            html += """<li><a href="/accounts/%s">%s</a></li>""" % (str(x), str(x))
    else:
        html +="<li><i>None right now.</i></li>"
    return HttpResponse(html)

def poll(request, id):
    ##GRAB POEM##
    poem = Poem.objects.get(pk=id) #certainly not optimized!
    formatted_facade = poem.html # not plaintext
    #add spinner to end of facade if necessary!
    if poem.stats.serving:
        spinner = '&nbsp;<img id="spinner" src="/static/img/arrows.gif" alt="..."/>'
        formatted_facade += spinner
    else:
        spinner = ''
    ##PING IT##
    ping, created = Ping.objects.get_or_create(user=request.user, poemstats=poem.stats) #put this user on the active users list
    ping.date = datetime.now()
    ping.save()
    ##UPDATE HTML FOR RESPONSE##
    if not poem.stats.loading:
        gif = 0
        if poem.stats.utterances==1:
            gif = 1
        elif poem.stats.utterances>=2:
            gif = 2
    else:
        gif = 777
        poem.stats.loading = poem.stats.loading - 1
        poem.stats.save()
    url = "%simg/meter/%i.gif" % ('/static/', gif)
    alt = "%i/2: Need %i more inputs to proceed." % (poem.stats.utterances, 2-poem.stats.utterances)
    metertag = '<img id="meterimg" src="%s" alt="%s">' % (url, alt)
    html = """<div class="plike" id="windeye"><div id="windeyetext">%s</div></div><div id="metercontainer"><div id="meter">%s</div></div>""" % (formatted_facade, metertag)
    return HttpResponse(html) #i don't know how to specify charset utf-8, whatever ...

def getpoem(request, id):
    poem = Poem.objects.get(pk=id) #certainly not optimized!
    htm = '<div class="plike" id="windeye"><div id="windeyetext">%s</div></div>' % poem.html
    return HttpResponse(htm)

def getpoems(request, number=13):
    poems = Poem.objects.order_by('-created_date', '-pub_date', 'title')
    recent_poems = poems[:number]
    html = " "
    for poem in recent_poems:
        if not poem.pub_date:
            post = "(open)"
        else: post = ""
        html += """<li><a href="/poems/view/%s">%s</a> %s</li>""" % ( str(poem.id), str(poem.title), post )
    return HttpResponse(html)

#def remaining(request, id):
#    pre = "&nbsp;"*5 #indent hack
#    response = pre
#    poem = Poem.objects.get(pk=id)
#    limit = poem.options.max_length
#    sofar = poem.stats.clusters_computed
#    left = limit - sofar
#    if left>0:
#        if poem.pub_date:
#            response += "This poem will be automatically published after %s more outputs." % str(left)
#        else:
#            response += "This poem was published manually, %s outputs short of its length limit." % str(left) 
#    else:
#        response += "This poem was automatically published when it reached its length limit."
#    return HttpResponse(response)

#############################################
####NORMAL VIEWS#############################
#############################################

def plain(request, id):
    """Printer/copy-paste---friendly view"""
    #poem = Poem.objects.get(pk=id) #certainly not optimized!
    #return render_response(request, 'plain.html', {"poem": poem})
    poem = Poem.objects.get(pk=id)
    #formatted_poem = poem.replace('\n', '<br/>').replace('\t', '&nbsp;'*4).replace('')
    formatted_facade = whitespace2html(poem.facade)
    now = datetime.now()
    accessed = now.strftime("%h. %d, %Y %I:%M%p")
    return render_response(request, 'plain.html', {"poem": poem, "formatted_facade": formatted_facade, "accessed": accessed})

def by(request, username):
    user = get_object_or_404(User, username=username)
    poems = Poem.objects.order_by('created_date', 'pub_date', 'title')
    their_poems = poems.filter(authors__pk=user.id)
    return render_response(request, 'by.html', {"poems": their_poems, "user": user})

def guest(request):
    from django.contrib.auth import authenticate, login
    last_user = User.objects.latest('date_joined')
    num = int(last_user.pk) + 1
    name = 'guest' + str(num)
    user = User.objects.create_user(name, name[-1]+'@dummy'+'.tseug', 'guest')
    user.save()
    prof = user.profile
    user = authenticate(username=name, password='guest')
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            raise Http404  
    else:
        raise Http404  

def view(request, id):
    """view/read poem (w/o dynamic updating) if not writable.
    but if writable, redirect to /write/"""
    poem = get_object_or_404(Poem, pk=id)
    if not poem.pub_date:
        return HttpResponseRedirect('/poems/write/%i/' % poem.id)
    else:
        poem.stats.views += 1
        poem.stats.save()
        poem.save()
        return render_response(request, 'read.html', {"poem": poem})
    
def kiosk(request, id):
    poem = get_object_or_404(Poem, pk=id)
    poem.stats.views += 1
    poem.stats.save()
    poem.save()
    return render_response(request, 'kiosk.html', {"poem": poem})

@login_required
def client(request, id):
    #placeholder: for checking whether poem is writable...
    poem = get_object_or_404(Poem, pk=id)
    full_house = False
    if not poem.pub_date:
        #if permission(request.user):
        if poem.options.max_simultaneous_authors != 0:
            if poem.stats.active_authors.count()>=poem.options.max_simultaneous_authors:
                full_house = True
            else:
                poem.authors.add(request.user) #add this user to the permanent author list
                initialPing(request, poem)
        else:
            poem.authors.add(request.user) #add this user to the permanent author list
            initialPing(request, poem)
        poem.stats.views += 1
        poem.stats.save()
        poem.save()
        return render_response(request,'dajax_client.html', {"poem": poem, "full_house": full_house})
    else:
        return HttpResponseRedirect('/poems/view/%i/' % poem.id)

@login_required
def create(request):
    from poetrynet.poems.forms import PartialPoemForm, PoemOptionsForm
    
    yourpoems = yours(request, clothe=False) #trust me, this is needed to put the yourpoems in the leftbar in this view specifically. coupled & ugly
    options_form_errors = False
    
    if request.method == 'POST':
        mainform = PartialPoemForm(request.POST, instance=Poem())
        optionsform = PoemOptionsForm(request.POST, instance=PoemOptions())
        if mainform.is_valid(): # All validation rules pass
            if optionsform.is_valid():
                options, created = PoemOptions.objects.get_or_create(**optionsform.cleaned_data)
                poem = mainform.save(commit=False)
                poem.basepickle = [ ]
                poem.phanopickle = [ [] ]
                poem.melopickle = [ [] ]
                poem.options = options
                poem.stats = PoemStats.objects.create()
                poem.creator = request.user
                ##GENERATE CODE & MODE_KEY##
                i = 0
                y = None
                code = []
                mode_key = []
                while i<10:
                    code.append( str(random.randint(0,9)) )
                    x = random.randint(1, 4)
                    if x==4:
                        if not y:
                            x = random.randint(1, 3)
                        else:
                            x = y
                    mode_key.append( str(x) )
                    y = x
                    i += 1
                poem.stats.code = "".join(code)
                poem.stats.mode_key = "".join(mode_key)
                ##OK SAVE##
                poem.stats.save()
                poem.save()
                return HttpResponseRedirect(reverse('Poem-write', args=(poem.id,))) # Redirect after POST -- FIX: needs to redirect to the proper client.
            else:
                options_form_errors = True
    else:
        mainform = PartialPoemForm(instance=Poem())
        optionsform = PoemOptionsForm(instance=PoemOptions())

    return render_to_response('create_poem.html', {'main_form': mainform, 'options_form': optionsform, 'options_form_errors': options_form_errors, 'yourpoems': yourpoems}, context_instance=RequestContext(request))

@login_required
def publish(request, id):                    
    p = get_object_or_404(Poem, pk=id)
    if not p.pub_date:
        if p.creator.id == request.user.id:
            p.pub_date = datetime.now()
            p.save()
    return HttpResponseRedirect('/poems/view/%i/' % p.id)