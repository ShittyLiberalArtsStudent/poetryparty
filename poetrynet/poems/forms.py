#from django import forms
from django import forms
from django.forms import ModelForm

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from poetrynet.poems.models import Poem, PoemOptions

class PartialPoemForm(ModelForm):
    class Meta: 
        model = Poem
        exclude = ('writable',
                   'created_date',
                   'pub_date',
                   'options',
                   'max_utterances',
                   'current_length_in_words',
                   'facade',
                   'active',
                   'active_authors',
                   'authors',
                   'stats',
                   'creator',
                   'html'
                   )
        
    title = forms.CharField(
               widget=forms.TextInput(attrs={'size':'40', 'autocomplete':"off" }),
               )
    welcome_message = forms.CharField(
           widget=forms.Textarea(attrs={'rows':'3', 'cols': '40'}),
           help_text="Displayed to each user upon joining the poem-session. Not displayed with the published poem.",
           )
    
class PoemOptionsForm(ModelForm):
    class Meta: 
        model = PoemOptions
        exclude = ('max_length')
    
    max_simultaneous_authors = forms.CharField(help_text="Maximum users that can participate in the creation of this poem at once. 0 means no limit!",
               widget=forms.TextInput(attrs={'size':'2'}),
               )