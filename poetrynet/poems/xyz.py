from django.core.exceptions import ObjectDoesNotExist

#from tasks import cookCluster
from poetrynet.poems.utils import UtteranceCucumber

def addUtterance(poem, input, user):
    #validate the utterance
    utterance_cucumber = UtteranceCucumber(input, user.id)
    if utterance_cucumber.valid:
        #get latest cluster
        c = Cluster.objects.filter(poem=poem.pk).latest() # <-- probably not optimized!!
        if c.crushstate>0:
            g = GhostUtterance(poem=poem, text=utterance_cucumber.text, pickle=utterance_cucumber, user=user)
            g.save()
        else:
            #bind utterance to cluster
            u = Utterance(text=utterance_cucumber.text, pickle=utterance_cucumber, user=user)
            try:
                lookup = Utterance.objects.get(clusters=c, text=utterance_cucumber.text)
            except ObjectDoesNotExist:
                u.save()
                intermediary = UtteranceMembership.objects.create(utterance=u, cluster=c)
                intermediary.save()
                poem.stats.utterances = c.utterance_set.count()
                if poem.stats.utterances >= poem.stats.max_utterances:
                    #from tasks import cookCluster # DYNAMICALLY IMPORT ITEM FROM TASKS.
                    task = cookCluster.delay(poem.id) #queue the task of crushin
                    poem.stats.loading = ( poem.stats.max_utterances*2 - 2) #this might need updating, later on ...
                poem.stats.save()
    #return utterance, which includes any messages that need to be sent to the user ...
    return utterance_cucumber