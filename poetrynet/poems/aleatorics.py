#############################
##LITE W8 ALEATORIC METHODS##
#############################

import random

def RNG(chance=50):
    """Simple random number generator that I made as a utility for aleatoric forks in the road.
    The chances it receives are on a scale of 100s -- i.e. 75=75% chance."""
    if chance:
        return random.random()<(chance/100.0)
    else: return False

def w8(item):
    if item==" ": return 200
    elif item=="\n": return 20
    elif item=="\t": return 10
    elif item=="\n\n": return 5
    
def WRC(seq, weight=w8):
    """ WEIGHTED RANDOM CHOICE from http://code.activestate.com/recipes/577363-weighted-random-choice/
    Returns a random element from ``seq``. The probability for each element
    ``elem`` in ``seq`` to be selected is weighted by ``weight(elem)``.

    ``seq`` must be an iterable containing more than one element.

    ``weight`` must be a callable accepting one argument, and returning a
    non-negative number. If ``weight(elem)`` is zero, ``elem`` will not be
    considered. 
        
    """ 
    weights = 0
    elems = [] 
    for elem in seq:
        w = weight(elem)     
        try:
            is_neg = w < 0
        except TypeError:    
            raise ValueError("Weight of element '%s' is not a number (%s)" %
                             (elem, w))
        if is_neg:
            raise ValueError("Weight of element '%s' is negative (%s)" %
                             (elem, w))
        if w != 0:               
            try:
                weights += w
            except TypeError:
                raise ValueError("Weight of element '%s' is not a number "
                                 "(%s)" % (elem, w))
            elems.append((weights, elem))
    if not elems:
        raise ValueError("Empty sequence")
    ix = bisect.bisect(elems, (random.uniform(0, weights), None))
    return elems[ix][1]