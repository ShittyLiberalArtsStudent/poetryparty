SPACE = " "
NEED_SPACE = """.,:;!?&+]})"`0123456789"""
EOS = ".;!?%"

import re
import string

#####################
##FORMATTING MACROS##
#####################

def stops2spaces(s):
    """Replaces "\t" and "\n" and with single spaces, and "%" with a period, so that linkgrammar doesn't get confused."""
    pattern = re.compile(r"[\t]+|[\n]+")
    y = pattern.search(s)
    while y is not None:
        s = s[:y.start()] + SPACE + s[y.end():]
        y = pattern.search(s)
    s = s.replace("%", ".")
    return s

##################
##PRETTYFICATION##
##################

def removeDoubleSpaces(x):
    """Remove sequences of EXACTLY two spaces."""
    pattern = re.compile(r"\b( {2})(\b)")
    x = pattern.sub(' ', x)
    return x

def groundFloatingPunctuation(x):
    """Ground floating punctuation: if "word     ." happens, change to "word.     "
    Repeat while the search still returns true!
    """
    pattern = re.compile(r"\b(\s+)([%s]+)" % re.escape(string.punctuation) )
    y = pattern.search(x)
    while y is not None:
        #print y
        new_fragment = "".join([ y.group(2) , y.group(1) ])
        x = x.replace(y.group(0), new_fragment)
        y = pattern.search(x)
    return x

def prettyJoin(q, starter=" ", finalprint=False, flag=True):
    #print "Pretty join's q: ", q
    #print "Pretty Join's STARTER", starter
    if q:
        x = ""
        def go(this, last):
            try:
                if last:
                    if last[-1].isspace():
                        return this
                    elif last[-1].isalnum() or last[-1] in NEED_SPACE:
                        if this[0].isspace():
                            return this
                        else:
                            return SPACE + this
                    return SPACE+this #<--if nothing else worked add a space
                else:
                    return this
            except IndexError:
                return SPACE
        try:
            last = starter[-1]
        except IndexError:
            last = " "
        for item in q:
            #print "ITEM", item
            #print "LAST", last
            got = go(item, last)
            x += got
            last = got
            #print "GOT", got
            #print "X", x
        if flag:
            x = groundFloatingPunctuation(x)
        if not finalprint:
            rx = re.compile(r'[ ]+')
            x = rx.sub(' ', x)
        else:
            x = removeDoubleSpaces(x)
        return str(x)
    else:
        return str(" ")

def rmEOS(x):
    """Remove end-of-sentence markers from a string."""
    r = re.compile(r'[%s]+' % EOS)
    x = r.sub('', x)
    return x