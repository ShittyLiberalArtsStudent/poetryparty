from django.conf.urls.defaults import *

import poetrynet.poems.views

urlpatterns = patterns('',
    url(r"write/(?P<id>\d+)/$", poetrynet.poems.views.client, name="Poem-write"),
    url(r"view/(?P<id>\d+)/$", poetrynet.poems.views.view, name="Poem-view"),
    url(r"kiosk/(?P<id>\d+)/$", poetrynet.poems.views.kiosk, name="Poem-kiosk"),
    url(r"publish/(?P<id>\d+)/$", poetrynet.poems.views.publish, name="Poem-publish"),
    url(r"poll/(?P<id>\d+)/$", poetrynet.poems.views.poll, name="Poem-poll"),
    url(r"by/(?P<username>\w+)/$", poetrynet.poems.views.by, name="Poem-by-author"),
    url(r"yours/$", poetrynet.poems.views.yours, name="Poem-yours"),
    url(r"getpoems/(?P<number>\d+)/$", poetrynet.poems.views.getpoems, name="Poem-getpoems"),
    url(r"getpoem/(?P<id>\d+)/$", poetrynet.poems.views.getpoem, name="Poem-getpoem"),
    url(r"userlist/(?P<id>\d+)/$", poetrynet.poems.views.userlist, name="Poem-userlist"),
    url(r"create/$", poetrynet.poems.views.create, name="Poem-create"),
    url(r"guest/$", poetrynet.poems.views.guest, name="guest"),
    url(r"print/(?P<id>\d+)/$", poetrynet.poems.views.plain, name="Poem-plain"),
)