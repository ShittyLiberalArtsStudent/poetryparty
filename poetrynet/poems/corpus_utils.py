try:
   import cPickle as pickle
except:
   import pickle

###################
##CORPUS HANDLING##
###################

#def fetchCorpus(corpNo=1):
#    """Either uses NLTK to get the corpus or opens from pickle."""
##    
#    need_to_build = False
#    
#    try:
#        print "loaded corpus without building"
#        pkl_file = open('corp%s.pkl' % corpNo, 'rb') #readable!
#        words = pickle.load(pkl_file)
#        need_to_build = False
#    except IOError:
#        need_to_build = True
#    except EOFError:
#        need_to_build = True
#        
#    if need_to_build:
#        print "NEED TO BUILD CORPUS"
#        pkl_file = open('corp%s.pkl' % corpNo, 'wb') #writable!
#        if corpNo==1:
#            from nltk.corpus import brown#
#            words = brown.words(categories=['editorial', 'religion', 'adventure'])
#        elif corpNo==2:
#            from nltk.corpus import brown
#            words = brown.words(categories=['news', 'fiction', 'romance', 'mystery', 'reviews', 'humor' ])
#        pickle.dump(words, pkl_file)
#        
#    pkl_file.close()
#    
#    return words

######################
##SENTENCE TOKENIZER##
######################

def fetchPunkt():
    """Either uses NLTK to train a sentence tokenizer from data in corpus (fetched by fetchCorpus()) or opens from pickle."""
    try:
        pkl_file = open('punk.pkl', 'rb') #readable!
        punkt = pickle.load(pkl_file)
    except IOError, e:
        pkl_file = open('punk.pkl', 'wb') #writable!
        curtailed_corpus = fetchCorpus()[15000]
        corpus_string = "".join(curtailed_corpus)
        from nltk.tokenize.punkt import PunktSentenceTokenizer
        punkt = PunktSentenceTokenizer(corpus_string, verbose=True)
        punkt.train(corpus_string, verbose=True)
        pickle.dump(punkt, pkl_file)
    pkl_file.close()
    return punkt




##############################################################################
## THIS ACTUALLY BELONGS IN NODEBOX_UTILS BUT I DEACTIVATED IT, SO ITS HERE ##
##############################################################################
def finishSentence(x, corpNo=1, dire=False):
    #Given an incomplete sentence, add some stuff to the end using ngrams for use with the grammar checker
    #initialize
    words = fetchCorpus(corpNo)
    print "Gonna finish sentence with corpus %i" %corpNo
    
    #main subfunction
    def go(x):
        try:
            first_occurence = words.index(x)
            
            period = words[first_occurence:].index('.')
            comma = words[first_occurence:].index(',')
            if comma<period and comma>first_occurence+1:
                end = comma
            else:
                end = period
                
            end = end + first_occurence
            start = len(x) + first_occurence
            results = words[start:end]
            
            def noneIfNotAlpha(item):
                """Weeds out punctuation marks that we don't want."""
                if item.isalpha():
                    return item
                else:
                    if item==":" or item==";":
                        return item
                    else:
                        return ""
                    
            stringy = " ".join( [noneIfNotAlpha(result) for result in results] )
            
            if dire:
                rx = re.compile(r'[.,:;!?&+)"]+')
                h = rx.search(stringy)
                if h:
                    stringy = stringy[:h.start()]
                
            return " " + stringy + '.' #add space at beginning & period at the end.
        
        except ValueError:
            return None
        
    results = go(x.lower())
    
    if not results: #if we're unable to find the word still ...
        results = go(x.title())
        if not results:
            #basic_word = choice( en.basic.words )
            #results = go(basic_word)
            #if not results:
            results = " or so it is said."
            
    return results