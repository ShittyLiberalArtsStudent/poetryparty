#this module contains some environment-variable info for settings.py.
#it has a hacky way of telling whether it is being accessed on dotcloud or not,
#and things roll out from there.ddd

SKEY='6!s7y!2p23s-5q^aw-&gtkjpw42^v8+3#*s1k*p(-jrlq$^r2g'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'poetrynet',                      # Or path to database file if using sqlite3.
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': 'outerverse9c0a52ef1d364518871e85c3ee4056e5outerverse',
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    }
}

MEDIA_ROOT = '/code/media/'
MEDIA_URL = '/media/'
STATIC_ROOT = '/code/static/'
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'

DAJAXICE_MEDIA_PREFIX = 'dajax'

TEMPLATE_DIRS = (
    "/code/templates",
)

BROKER_HOST = "broker"
BROKER_PORT = 5672
BROKER_USER = "guest"
BROKER_PASSWORD = 'guest'
BROKER_VHOST = "/"
