from django.conf.urls.defaults import patterns, include, url
from django.conf import settings

from dajaxice.core import dajaxice_autodiscover
dajaxice_autodiscover()

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'poetrynet.views.home', name='home'),
    # url(r'^poetrynet/', include('poetrynet.foo.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/$', settings.MEDIA_ROOT),
    url(r'^accounts/', include('userena.urls')),
    url(r'^$', 'poetrynet.frontpage.views.home', name='home'),
    url(r'^contact/$', 'poetrynet.frontpage.views.contact', name='contact'),
    url(r'^instructions/$', 'poetrynet.frontpage.views.instructions', name='instructions'),
    url(r'^allpoems/$', 'poetrynet.frontpage.views.allpoems', name='allpoems'),
    url(r'^poems/', include('poetrynet.poems.urls')),
    #Dajax URLS
    url(r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
)

