from django.db import models
from django.contrib.auth.models import User
from userena.models import UserenaBaseProfile


class UserProfile(UserenaBaseProfile):
    user = models.OneToOneField(User,
                                unique=True,
                                verbose_name=('user'),
                                related_name='my_profile')



User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])