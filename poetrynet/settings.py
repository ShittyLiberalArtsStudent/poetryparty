# Django settings for poetrynet project.

from funcy import merge

from poetrynet import confidential


DEBUG = confidential.DEBUG
TEMPLATE_DEBUG = confidential.TEMPLATE_DEBUG

ADMINS = (
    ('Michael Floering', 'michaelfloering@gmail.com'),
)

MANAGERS = ADMINS

# i use an extra py file confidential.py to store confidential info, you will not find this on git

DATABASES = {

    'default': merge(
        confidential.DATABASES['default'], {
            'ENGINE': 'django.db.backends.mysql',
            # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'poetrynet',  # Or path to database file if using sqlite3.
            'HOST': 'db',# os.environ['DB_PORT_3306_TCP_ADDR'],
            'PORT': '3306',  # Set to empty string for default. Not used with sqlite3.
        }),
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = confidential.MEDIA_ROOT

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = confidential.MEDIA_URL

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = confidential.STATIC_ROOT

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = confidential.STATIC_URL

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = confidential.ADMIN_MEDIA_PREFIX

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = confidential.SKEY

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = ("django.contrib.auth.context_processors.auth",
                               "django.core.context_processors.debug",
                               "django.core.context_processors.i18n",
                               "django.core.context_processors.media",
                               "django.core.context_processors.static",
                               "django.core.context_processors.request",
                               "django.contrib.messages.context_processors.messages")

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'poetrynet.urls'

TEMPLATE_DIRS = confidential.TEMPLATE_DIRS

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',

    'userena', 'guardian', 'easy_thumbnails',  #userena requires these three things
    'poetrynet.accounts',
    'djcelery',

    'dajaxice',
    'dajax',  #pretty sure you don't import 'dajax' actually

    'poetrynet.frontpage',
    'poetrynet.poems',
)

DAJAXICE_MEDIA_PREFIX = confidential.DAJAXICE_MEDIA_PREFIX
DAJAXICE_CACHE_CONTROL = 10 * 24 * 60 * 60

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}



#Adding some things to the settings because Userena wants me to and I want to use Userena
AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

ANONYMOUS_USER_ID = -1

AUTH_PROFILE_MODULE = 'accounts.userprofile'

LOGIN_REDIRECT_URL = '/poems/by/%(username)s/'
LOGIN_URL = '/accounts/signin/'
LOGOUT_URL = '/accounts/signout/'

#Userena Settings
USERENA_DISABLE_PROFILE_LIST = True
USERENA_ACTIVATION_REQUIRED = False
USERENA_MUGSHOT_DEFAULT = 'monsterid'

#Django-celery (djcelery) stuff (see here:
# http://django-celery.readthedocs.org/en/latest/getting-started/first-steps-with-django.html)
import djcelery

djcelery.setup_loader()
BROKER_HOST = confidential.BROKER_HOST
BROKER_PORT = confidential.BROKER_PORT
BROKER_USER = confidential.BROKER_USER
BROKER_PASSWORD = confidential.BROKER_PASSWORD
BROKER_VHOST = confidential.BROKER_VHOST
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERY_IMPORTS = ("poetrynet.poems.tasks", )
CELERY_RESULT_BACKEND = "amqp"
