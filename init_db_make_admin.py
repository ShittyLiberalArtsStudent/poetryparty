#!/usr/bin/env python
from uuid import uuid4

from django.contrib.auth.models import User

u, created = User.objects.get_or_create(username='SeaThingsSeaThingSeaThings')
if created:
    password = 'dadaconf0-' + str(uuid4())
    print "[!] password for {} is: {!r}".format(u.username, password)
    u.set_password(password)
    u.is_superuser = True
    u.is_staff = True

    u.profile.save()

    u.save()

else:
    print '[.] admin user exists, moving on'